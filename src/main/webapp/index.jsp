<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>Municipcgms</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  <!--  
  
  body {
      font: 400 15px Lato, sans-serif;
      line-height: 1.8;
      color: #818181;
  }
  
  .carousel-inner>.item>img {
	width: 1500px;
	height: 500px;
	border: 5px solid black;
} 
  .how-it-works {
    background: white;
	padding-bottom: 30px;
}

.board{
	/*width: 75%;
	margin: 60px auto;
	margin-bottom: 0;
	box-shadow: 10px 10px #ccc,-10px 20px #ddd;*/
}
.board .nav-tabs {
	position: relative;
	/* border-bottom: 0; */
	/* width: 80%; */
	margin: 40px auto;
	margin-bottom: 0;
	box-sizing: border-box;

}

.board > div.board-inner > .nav-tabs {
	border: none;
}

p.narrow{
	width: 60%;
	margin: 10px auto;
}

.liner{
	height: 2px;
	background: #ddd;
	position: absolute;
	width: 80%;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: 50%;
	z-index: 1;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
	color: #555555;
	cursor: default;
	/* background-color: #ffffff; */
	border: 0;
	border-bottom-color: transparent;
	outline: 0;
}

span.round-tabs{
	width: 70px;
	height: 70px;
	line-height: 70px;
	display: inline-block;
	border-radius: 100px;
	background: white;
	z-index: 2;
	position: absolute;
	left: 0;
	text-align: center;
	font-size: 25px;
}

span.round-tabs.one{
	border: 2px solid #ddd;
	color: #ddd;
}

li.active span.round-tabs.one, li.active span.round-tabs.two, li.active span.round-tabs.three, li.active span.round-tabs.four, li.active span.round-tabs.five {
	background: #69cb95 !important;
	border: 2px solid #69cb95;
	color: #fff;
}

span.round-tabs.two{
	border: 2px solid #ddd;
	color: #ddd;
}

span.round-tabs.three{
	border: 2px solid #ddd;
	color: #ddd;
}

span.round-tabs.four{
	border: 2px solid #ddd;
	color: #ddd;
}

span.round-tabs.five{
	border: 2px solid #ddd;
	color: #ddd;
}

.nav-tabs > li.active > a span.round-tabs{
	background: #fafafa;
}
.nav-tabs > li {
	width: 20%;
}

.nav-tabs > li a{
	width: 70px;
	height: 70px;
	margin: 20px auto;
	border-radius: 100%;
	padding: 0;
}

.nav-tabs > li a:hover{
	background: transparent;
}

.tab-content{
}
.tab-pane{
	position: relative;
	padding-top: 50px;
}

.btn-outline-rounded{
	padding: 10px 40px;
	margin: 20px 0;
	border: 2px solid transparent;
	border-radius: 25px;
}

.btn.green{
	background-color:#69cb95;
	/*border: 2px solid #5cb85c;*/
	color: #ffffff;
}


ul#navigation {
    position: fixed;
    margin: 0px;
    padding: 0px;
    top: 70px;
    left: 0px;
    list-style:no
    z-index:9999;
}
span{font-size:1.3em !important;}
span i{font-size:1.5em !important; padding-top:15px !important ;}
ul#navigation li {
    width: 100px;
}
ul#navigation li a {
    display: block;
    margin-left: -2px;
    width: 100px;
    height: 70px;    
    background-color:#69cb95;
    background-repeat:no-repeat;
    background-position:center center;
    border:1px solid black;
    -moz-border-radius:0px 10px 10px 0px;
    -webkit-border-bottom-right-radius: 10px;
    -webkit-border-top-right-radius: 10px;
    -khtml-border-bottom-right-radius: 10px;
    -khtml-border-top-right-radius: 10px;
    /*-moz-box-shadow: 0px 4px 3px #000;
    -webkit-box-shadow: 0px 4px 3px #000;
    */
    opacity: 0.6;
    filter:progid:DXImageTransform.Microsoft.Alpha(opacity=60);
}

@media( max-width : 585px ){

	.board {
		width: 90%;
		height:auto !important;
	}
	span.round-tabs {
		font-size:16px;
		width: 50px;
		height: 50px;
		line-height: 50px;
	}
	.tab-content .head{
		font-size:20px;
	}
	.nav-tabs > li a {
		width: 50px;
		height: 50px;
		line-height:50px;
	}

	li.active:after {
		content: " ";
		position: absolute;
		left: 35%;
	}

	.btn-outline-rounded {
		padding:12px 20px;
	}
}
	 
  h2 {
      font-size: 24px;
      text-transform: uppercase;
      color: #303030;
      font-weight: 600;
      margin-bottom: 30px;
  }
  h4 {
      font-size: 19px;
      line-height: 1.375em;
      color: #303030;
      font-weight: 400;
      margin-bottom: 30px;
  }  
  
  .container-fluid {
      padding: 60px 50px;
 
  }
  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo-small {
      color: black;
      font-size: 50px;
  }
  .logo {
      color: black;
      font-size: 200px;
  }
  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail img {
      width: 100%;
      height: 100%;
      margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
      background-image: none;
      color: black;
  }
  .carousel-indicators li {
      border-color: black;
  }
  .carousel-indicators li.active {
      background-color: black;
  }
  .item h4 {
      font-size: 19px;
      line-height: 1.375em;
      font-weight: 400;
      font-style: italic;
      margin: 70px 0;
  }
  .item span {
      font-style: normal;
  }
  .panel {
      border: 1px solid black; 
      border-radius:0 !important;
      transition: box-shadow 0.5s;
  }
  .panel:hover {
      box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
      border: 1px solid black;
      background-color: #fff !important;
      color: #f4511e;
  }
  .panel-heading {
      color: #fff !important;
      background-color: black !important;
      padding: 25px;
      border-bottom: 1px solid transparent;
      border-top-left-radius: 0px;
      border-top-right-radius: 0px;
      border-bottom-left-radius: 0px;
      border-bottom-right-radius: 0px;
  }
  .panel-footer {
      background-color: white !important;
  }
  .panel-footer h3 {
      font-size: 32px;
  }
  .panel-footer h4 {
      color: #aaa;
      font-size: 14px;
  }
  .panel-footer .btn {
      margin: 15px 0;
      background-color: black;
      color: #fff;
  }
  .navbar {
      margin-bottom: 0;
      background-color: black;
      z-index: 9999;
      border: 0;
      font-size: 12px !important;
      line-height: 1.42857143 !important;
      letter-spacing: 4px;
      border-radius: 0;
      font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
      color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: black !important;
      background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  footer .glyphicon {
      font-size: 20px;
      margin-bottom: 20px;
      color: black;
  }
  .slideanim {visibility:hidden;}
  .slide {
      animation-name: slide;
      -webkit-animation-name: slide;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
        width: 100%;
        margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
        font-size: 150px;
    }
  }
  
  -->
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#myPage">Clean City</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">ABOUT</a></li>
        <li><a href="outdoor.do?action=outdoorgarbage">OUTDOORGARBAGE</a></li>
        <li><a href="resident.do?action=frontresident">RESIDENTREGISTRATION</a></li>
        <li><a href="login.do?action=loginview">LOGIN</a></li>
      </ul>
    </div>
  </div>
</nav>
<br><br>



<!-- slide -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>


			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox" style="width: 1500px height: 500px border: 5px solid black">
				<div class="item active">
					<img src="image/flag.jpg" alt="New York" width="100%">
					<div class="carousel-caption">
				
					</div>
				</div>

				<div class="item">
					<img src="image/swach2.jpg" alt="Chicago" width="100%">
					<div class="carousel-caption">
				
					</div>
				</div>

				<div class="item">
					<img src="image/swach3.jpg" alt="Los Angeles" width="100%">
					<div class="carousel-caption">
											
			</div>
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"> <span class="#" aria-hidden="true"></span> <span
				class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"> <span class="#" aria-hidden="true"></span> <span
				class="sr-only">Next</span>
			</a>
		</div>

<!-- Container (Portfolio Section) -->

<div id="portfolio" class="container-fluid text-center bg-grey">
  
  <h4>What we do</h4>
  <div class="row slideanim">
    <div class="col-sm-4">
      <div class="thumbnail">
        <img src="image/dustbin1.jpg" alt="Paris" width="400" height="300">
        <ul>
      <li>  <p style="font-style:italic; color:black; ">Keep separate containers for dry and wet waste..<p></li>
      <li>  <p style="font-style:italic; color:black; "> Keep a paper bag for throwing the sanitary waste.c</p></li>
       <li> <p style="font-style:italic; color:black; ">Send wet waste out of your home daily.</p></li>
        </ul>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail text-center ">
        <img src="image/dustbin2.jpg" alt="New York" width="400" height="300">
        <p style="color: black; font-style: italic;">"Swachh Bharat Abhiyan- <br>is a campaign in India that aims to clean up the 
        streets<br>roads and infrastructure of India's cities<br>smaller towns, and
         rural areas"</p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="thumbnail text-center ">
        <img src="image/dustbin3.jpg" alt="New York" width="400" height="300">
        <p style="color: black; font-style: italic;">"Swachh Bharat Abhiyan- <br>is a campaign in India that aims to clean up the 
        streets<br>roads and infrastructure of India's cities<br>smaller towns, and
         rural areas"</p>
      </div>
    </div>
  </div> 

  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you within 24 hours.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span>Banglore</p>
      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
    </div>
    <div class="col-sm-7 slideanim">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit">Send</button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p> <a href="#"></a> <a href="http://www.inoesissolutions.com/" rel="noopener norreferrer" target="_blank">Powered By i-Noesis Solutions</a></p>

</footer>

<script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
});



$(function() {
    $('#navigation a').stop().animate({'marginLeft':'-55px'},1000);

    $('#navigation > li').hover(
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-2px'},200);
        },
        function () {
            $('a',$(this)).stop().animate({'marginLeft':'-55px'},200);
        }
    );
});


</script>

</body>
</html>

