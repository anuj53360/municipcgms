
<%--  <jsp:include page="../header/header.html" />--%>
<div class="container">

  <h2>Resident</h2>
  <%
if(request.getAttribute("residentcreatemessage")!=null){%>
  <p color="red"><%= request.getAttribute("residentcreatemessage") %></p>
  <% } %>
  <div class="col-sm-10">
  <form action="ResidentController" method="post" enctype="multipart/form-data" name="resident-regis-form" id="resident-regis-form" role="form">
  <div class="form-group">
    <label for="cmcNumber">CMC Number</label>
    <input type="text" class="form-control" name="resNumber" id="cmcNumber" placeholder="Enter CMC No">
  </div>
  <div class="form-group">
    <label for="firstName">First Name</label>
    <input type="text" class="form-control" name="resFname" id="firstName"  placeholder="Enter First Name">    
  </div>
  <div class="form-group">
    <label for="midleName">Midle Name</label>
    <input type="text" class="form-control" name="resMname" id="midleName"  placeholder="Enter Midle Name">    
  </div>
  <div class="form-group">
    <label for="lastName">Last Name</label>
    <input type="text" class="form-control" name="resLname" id="lastName"  placeholder="Enter Last Name">    
  </div>
  <div class="form-group">
    <label for="cmcMobile">Mobile</label>
    <input type="number" class="form-control" name="resMobile" id="cmcMobile"  placeholder="Enter Number">    
  </div>
   <div class="form-group">
    <label for="emailId">Email address</label>
    <input type="email" class="form-control" name="resEmail" id="emailId" placeholder="Enter Email">
     </div>
     <div class="dropdown">
      <label for="identityType">Identity Type</label>
      <select class="form-control mrgn-bttm-md"  id="identityType" name="idType" >
      <option hidden="">choose identity</option>
      <option value="adhaar">Adhar Number</option>
       <option value="pan">PAN Number</option>
        </select>
     </div> 
     <div class="form-group">    
      <label for="identityType">Identity Value</label>
    <input type="text" class="form-control" name="idValue" id="identityValue"  placeholder="Enter your Identity Value">    
  </div>
  <div class="form-group">
    <label for="photoPath">Photo</label>
    <input type="file" class="form-control" name="resPhoto" id="photoPath"  placeholder="Upload Resident Photo">    
  </div>
  <div class="form-group">
    <label for="ward">Ward</label>
    <input type="text" class="form-control" name="resward" id="ward"  placeholder="Enter Resident Ward">    
  </div>
  
    <div class="form-group">
     <input type="hidden" class="form-control" name="action" value="createresident"/>    
  </div>
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>

