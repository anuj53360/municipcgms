<!DOCTYPE html>
<html lang="en">
<head>
<!-- Theme Made By www.w3schools.com - No Copyright -->
<title>Muncip</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet" type="text/css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>


</script>
	<style>
	    body,html{
		height: 100%;
		margin:0;
		padding:0;
		overflow-x:hidden;   
	}

	/* remove outer padding */
	.main .row{
		padding: 0px;
		margin: 0px;
	}

	/*Remove rounded coners*/

	nav.sidebar.navbar {
		border-radius: 0px;
	}

	nav.sidebar, .main{
		-webkit-transition: margin 200ms ease-out;
	    -moz-transition: margin 200ms ease-out;
	    -o-transition: margin 200ms ease-out;
	    transition: margin 200ms ease-out;
	}

	/* Add gap to nav and right windows.*/
	.main{
		padding: 10px 10px 0 10px;
	}

	/* .....NavBar: Icon only with coloring/layout.....*/

	/*small/medium side display*/
	@media (min-width: 768px) {

		/*Allow main to be next to Nav*/
		.main{
			position: absolute;
			width: calc(100% - 40px); /*keeps 100% minus nav size*/
			margin-left: 40px;
			float: right;
		}

		/*lets nav bar to be showed on mouseover*/
		nav.sidebar:hover + .main{
			margin-left: 200px;
		}

		/*Center Brand*/
		nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
			margin-left: 0px;
		}
		/*Center Brand*/
		nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
			text-align: center;
			width: 100%;
			margin-left: 0px;
		}

		/*Center Icons*/
		nav.sidebar a{
			padding-right: 13px;
		}

		/*adds border top to first nav box */
		nav.sidebar .navbar-nav > li:first-child{
			border-top: 1px #e5e5e5 solid;
		}

		/*adds border to bottom nav boxes*/
		nav.sidebar .navbar-nav > li{
			border-bottom: 1px #e5e5e5 solid;
		}

		/* Colors/style dropdown box*/
		nav.sidebar .navbar-nav .open .dropdown-menu {
			position: static;
			float: none;
			width: auto;
			margin-top: 0;
			background-color: transparent;
			border: 0;
			-webkit-box-shadow: none;
			box-shadow: none;
		}

		/*allows nav box to use 100% width*/
		nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
			padding: 0 0px 0 0px;
		}

		/*colors dropdown box text */
		.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
			color: #777;
		}

		/*gives sidebar width/height*/
		nav.sidebar{
			width: 200px;
			height: 100%;
			margin-left: -160px;
			float: left;
			z-index: 8000;
			margin-bottom: 0px;
		}

		/*give sidebar 100% width;*/
		nav.sidebar li {
			width: 100%;
		}

		/* Move nav to full on mouse over*/
		nav.sidebar:hover{
			margin-left: 0px;
		}
		/*for hiden things when navbar hidden*/
		.forAnimate{
			opacity: 0;
		}
	}

	/* .....NavBar: Fully showing nav bar..... */

	@media (min-width: 1330px) {

		/*Allow main to be next to Nav*/
		.main{
			width: calc(100% - 200px); /*keeps 100% minus nav size*/
			margin-left: 200px;
		}

		/*Show all nav*/
		nav.sidebar{
			margin-left: 0px;
			float: left;
		}
		/*Show hidden items on nav*/
		nav.sidebar .forAnimate{
			opacity: 1;
		}
	}

	nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
		color: #CCC;
		background-color: transparent;
	}

	nav:hover .forAnimate{
		opacity: 1;
	}
	section{
		padding-left: 15px;
	}
	
	
	</style>
<body>
<%-- <jsp:include page="../header/headerL.html" /> --%>

<script>
function htmlbodyHeightUpdate(){
	var height3 = $( window ).height()
	var height1 = $('.nav').height()+50
	height2 = $('.main').height()
	if(height2 > height3){
		$('html').height(Math.max(height1,height3,height2)+10);
		$('body').height(Math.max(height1,height3,height2)+10);
	}
	else
	{
		$('html').height(Math.max(height1,height3,height2));
		$('body').height(Math.max(height1,height3,height2));
	}
	
}
$(document).ready(function () {
	htmlbodyHeightUpdate()
	$( window ).resize(function() {
		htmlbodyHeightUpdate()
	});
	$( window ).scroll(function() {
		height2 = $('.main').height()
			htmlbodyHeightUpdate()
	});
	

	$("#collectGarbage").click(function(){
	//	alert("11111111");
	$(".rightSection").hide();
		$.ajax({
			url: "ViewResidentCmcNo",
			method:"post",
			success:function(data){
//				alert(data.cmcno);
				$('#garbagetab').show();
				list=data.cmcno;
				var htmlString;
				$.each(list, function(i, item){
					htmlString=htmlString+"<tr><td><input type='text' name='" + list[i] + "'value='"+ 	list[i] +"'readonly='readonly'/> </td><td> <input type='checkbox' name='" +list[i]+"c"+"'/></td></tr>";
				});
				$("#residentCmcNo").html(htmlString);
			}
		});
	});
	
});

</script>
<nav class="navbar navbar-inverse sidebar" role="navigation">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
			<%
			String name= (String)session.getAttribute("uname");
				int y=name.length();
				//System.out.println(y+"x");
				String sub=name.substring(1, y);
				//System.out.println(sub+"sub");
				char namefirstchar=name.charAt(0);
				//System.out.println(namefirstchar);
				String s=String.valueOf(namefirstchar);
				String upper=s.toUpperCase();
				String newstring=upper.concat(sub);
			%>
			<a class="navbar-brand" href="#"><%=newstring %> (T)</a>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<!-- <li class="active"><a href="#">Dashboard<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li> -->
				<li ><a href="#garbagetab" id="collectGarbage">Collect garbage<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-trash"></span></a></li>
				<li><a href="changepassword.do?action=changepasswordtransporter">Change
							Password<span style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span>
					</a></li>
					<li ><a href="logout.do?action=logout">Logout<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-off"></span></a></li>
					</ul>
				
		</div>
	</div>
</nav>
<div class="rightSection">
		<% 
String pageincl="";
	if(request.getAttribute("page")!=null){
	String pg=request.getAttribute("page").toString();
	
	
	 if (pg.equals("changepassword")) {
		pageincl = "changepassword.jsp";
	}
}
		%>
		<%
	if(!pageincl.isEmpty()){
		
	
	%>

		<div class="col-sm-9">
			<jsp:include page='<%=pageincl %>' />
		</div>
		<%} %>
	</div>


<div class="container" id="garbagetab" hidden> 
<div class="col-sm-9">
<h1 style="text-align: center;">View Collect Garbage</h1>
<div class="col-sm-3"></div>
<div align="center" class="col-sm-6">

  <div class="table table-responsive">   
  <form action="collectgc.do">
  <table class="table table-striped table-inverse" >
  <thead>
    <tr>
      <th>cmcno</th>
      <th>garbage collect</th>
    </tr>
  </thead>
  <tbody id="residentCmcNo">
  </tbody>
 
</table>

<input type="hidden" name="action" value="collectgc">
<input type="submit" class="btn btn-default" value="Submit">
</form>
</div>

</div>
<div class="col-sm-3"></div>
</div>
 </div>
<div style="text-align:center; color: blue; ">
<%

if(request.getAttribute("status")!=null){%>
	<h3> <%=request.getAttribute("status") %></h3>
<% }
%>
</div>
 </body>
</html>
