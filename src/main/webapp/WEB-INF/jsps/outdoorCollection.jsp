<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script src="js/outdoorgarbage.js"></script>
</head>
<body>
  <jsp:include page="../header/header.html" />
<div class="container">
<h2>OutDoor Collection</h2>
 <%
if(request.getAttribute("message")!=null){%>
  <p color="red"><%= request.getAttribute("message") %></p>
  <% } %>

 <div class="col-sm-10">
 <form action="OutdoorCollection" method="post" enctype="multipart/form-data" name="outdoor-regis-form" id="outdoor-regis-form" role="form">
  <div class="form-group">
    <label for="outGarbagepic">Select Photo</label>
    <input type="file" class="form-control" name="outdoorpic" id="outGarbagepic" placeholder="Upload Garbage Photo">    
  </div>
<div class="dropdown" id="dropdown">
      <label for="dropdown">Select Ward</label>
      <select class="form-control mrgn-bttm-md"  id="wardDropdown" name="dropdown" >
     
        </select>
     </div>
 <div class="form-group">
    <label for="outdoorLocation">Location</label>
    <input type="text" class="form-control" name="outLoction" id="outdoorLocation"  placeholder="Enter Garbage Place Location">    
  </div>
  
  <div class="form-group">
    <input type="hidden" class="form-control" name="action" id="outdoorLocation" value="outdoorcollection">    
  </div>
 
  <button type="submit" class="btn btn-primary">Login</button>
 </form>
 </div>
</div>
</body>
</html>