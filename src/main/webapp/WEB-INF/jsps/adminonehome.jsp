<!DOCTYPE html>
<html lang="en">
<head>
<!-- Theme Made By www.w3schools.com - No Copyright -->
<title>Muncip</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet" type="text/css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/script.js"></script>
<script>

 $(document).ready(function(){
	$("#tab").hide();
	$("#residentwardDropdown").change(function(){
		$.ajax({
			url: "ViewDetails",
			method:"post",
			data:{action:$("#residentwardDropdown").val()},
			success:function(data){
				list = data.details;
				$("#tab").show();
				var htmlString;
				$.each(list, function(i, item){
					htmlString=htmlString+"<tr><td>" + list[i].cmcno + "</td><td>" + list[i].name +"</td><td>"+list[i].ward +"</td></tr>";
				}
				
				);
				$("#residenttablebody").html(htmlString);
			}
			
		});
	});
	
	$("#submit123").click(function(){
		$('.rightSection').hide();
		$('#transporter').hide();
		$.ajax({
			url: "ViewDetails",
			method:"post",
			data:{action:$("#submit123").attr("value")},
			success:function(data){
				$('div#viewresident').show();
				list = data.wards;
				var htmlString= "<option hidden=''>choose ward</option>"+" <option value='all'>all</option>";
			     
				$.each(list, function(i, item){
					 htmlString=htmlString+"<option value="+list[i].ward+">"+list[i].ward+"</option>"
					
				});
				$("#residentwardDropdown").html(htmlString);
			}
		});
	});
}); 
</script>

<script>

  $(document).ready(function(){
		
	  $("#submit12").click(function(){
		  $('.rightSection').hide();
			$("#viewresident").hide();
			$.ajax({
				url: "TransporterDetails",
				method:"post",
				data:{action:$("#submit12").attr("value")},
				success:function(data){
					$('div#transporter').show();
					list = data.details;
					var htmlString= "<option value=''>choose Transporter</option>";
				     
					$.each(list, function(i, item){
						 htmlString=htmlString+"<option value="+list[i].mobile+">"+list[i].name+"</option>"
						
					});
					$("#transporterDropdown").html(htmlString);
				}
			});
		});
	
	
	$("#submit12").click(function(){
	
		$.ajax({
			url: "ViewDetails",
			method:"post",
			data:{action:$("#submit12").attr("value")},
			success:function(data){
				list = data.wards;
				var htmlString= "<option value=''>choose ward</option>";
			     
				$.each(list, function(i, item){
					 htmlString=htmlString+"<option value="+list[i].ward+">"+list[i].ward+"</option>"
					
				});
				$("#wardDropdown").html(htmlString);
			}
		});
	});
	
	$("#wardDropdown").change(function(){
		if($(this).val()){
			$("#spanchooseward").text("");
		}
	});
	
	$("#transporterDropdown").change(function(){
		if($(this).val()){
			$("#spanchoosetransporter").text("");
		}
	});
	$("#transtowardAssign").click(function(){
		//alert($("#wardDropdown").val());
		var z= $("#dat").val();
		
		if(!$("#wardDropdown").val()){
			$("#spanchooseward").text("Please Choose any one");
		}
		if(!$("#transporterDropdown").val()){
			$("#spanchoosetransporter").text("Please Choose any one");			
		}
		
		if($("#wardDropdown").val() && $("#transporterDropdown").val()){
			$.ajax({
				url: "transportertoward.do",
				method:"post",
				data:{action:$("#action20").attr("value"),dropdown:$("#wardDropdown").val(),dropdown1:$("#transporterDropdown").val(),dat:$("#dat").val()},
				success:function(data){
					$("#success20").text("Successfully Submitted...");
				}
			});
		}	
		
	});
	
	
	$("#createtransporter").click(function(){
	$('#transporter').hide();
	});
	
	$("#createresident").click(function(){
		$('#transporter').hide();
	});
	$.ajax({
		url : "outdoor.do",
		method : "post",
		data : {actiononeout : "notification"},
		success : function(data) {
			$("#notification").text(data.notify);
			
		}
	});
	
    $("#outdoor").hide();
	
	$("#submitoutdoor").click(function(){
		
	$.ajax({
		url : "outdoorresult.do",
		method : "post",
		data : {actiononeresult : "outdoorward"},
		success : function(data) {
			list = data.outdoordetails;
			$("#outdoor").show();
			var htmlString;
			$.each(list, function(i, item) {
				htmlString = htmlString
						+ "<tr><td>"
						+ "<img src='"+list[i].imagepath+"'/>"
						+ "</td><td>"
						+ list[i].ward
						+ "</td></tr>";
			}

			);
			$("#outdoortablebody").html(
					htmlString);
		}

	});
	
	});
	
  }); 
</script>
<style>
body, html {
	height: 100%;
	margin: 0;
	padding: 0;
	overflow-x: hidden;
}

/* remove outer padding */
.main .row {
	padding: 0px;
	margin: 0px;
}

/*Remove rounded coners*/
nav.sidebar.navbar {
	border-radius: 0px;
}

nav.sidebar, .main {
	-webkit-transition: margin 200ms ease-out;
	-moz-transition: margin 200ms ease-out;
	-o-transition: margin 200ms ease-out;
	transition: margin 200ms ease-out;
}

/* Add gap to nav and right windows.*/
.main {
	padding: 10px 10px 0 10px;
}

/* .....NavBar: Icon only with coloring/layout.....*/

/*small/medium side display*/
@media ( min-width : 768px) {
	/*Allow main to be next to Nav*/
	.main {
		position: absolute;
		width: calc(100% - 40px); /*keeps 100% minus nav size*/
		margin-left: 40px;
		float: right;
	}

	/*lets nav bar to be showed on mouseover*/
	nav.sidebar:hover+.main {
		margin-left: 200px;
	}

	/*Center Brand*/
	nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand
		{
		margin-left: 0px;
	}
	/*Center Brand*/
	nav.sidebar .navbar-brand, nav.sidebar .navbar-header {
		text-align: center;
		width: 100%;
		margin-left: 0px;
	}

	/*Center Icons*/
	nav.sidebar a {
		padding-right: 13px;
	}

	/*adds border top to first nav box */
	nav.sidebar .navbar-nav>li:first-child {
		border-top: 1px #e5e5e5 solid;
	}

	/*adds border to bottom nav boxes*/
	nav.sidebar .navbar-nav>li {
		border-bottom: 1px #e5e5e5 solid;
	}

	/* Colors/style dropdown box*/
	nav.sidebar .navbar-nav .open .dropdown-menu {
		position: static;
		float: none;
		width: auto;
		margin-top: 0;
		background-color: transparent;
		border: 0;
		-webkit-box-shadow: none;
		box-shadow: none;
	}

	/*allows nav box to use 100% width*/
	nav.sidebar .navbar-collapse, nav.sidebar .container-fluid {
		padding: 0 0px 0 0px;
	}

	/*colors dropdown box text */
	.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
		color: #777;
	}

	/*gives sidebar width/height*/
	nav.sidebar {
		width: 200px;
		height: 100%;
		margin-left: -160px;
		float: left;
		z-index: 8000;
		margin-bottom: 0px;
	}

	/*give sidebar 100% width;*/
	nav.sidebar li {
		width: 100%;
	}

	/* Move nav to full on mouse over*/
	nav.sidebar:hover {
		margin-left: 0px;
	}
	/*for hiden things when navbar hidden*/
	.forAnimate {
		opacity: 0;
	}
}

/* .....NavBar: Fully showing nav bar..... */
@media ( min-width : 1330px) {
	/*Allow main to be next to Nav*/
	.main {
		width: calc(100% - 200px); /*keeps 100% minus nav size*/
		margin-left: 200px;
	}

	/*Show all nav*/
	nav.sidebar {
		margin-left: 0px;
		float: left;
	}
	/*Show hidden items on nav*/
	nav.sidebar .forAnimate {
		opacity: 1;
	}
}

nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus
	{
	color: #CCC;
	background-color: transparent;
}

nav:hover .forAnimate {
	opacity: 1;
}

section {
	padding-left: 15px;
}
</style>
   <style>
  .error

{

border-color:Red;


}
label.error{
color:Red; 
}
.badge {
    background-color: red;
}
  </style>
<body>
	<%-- <jsp:include page="../header/headerL.html" /> --%>

	<script>
function htmlbodyHeightUpdate(){
	var height3 = $( window ).height()
	var height1 = $('.nav').height()+50
	height2 = $('.main').height()
	if(height2 > height3){
		$('html').height(Math.max(height1,height3,height2)+10);
		$('body').height(Math.max(height1,height3,height2)+10);
	}
	else
	{
		$('html').height(Math.max(height1,height3,height2));
		$('body').height(Math.max(height1,height3,height2));
	}
	
}
$(document).ready(function () {
	htmlbodyHeightUpdate()
	$( window ).resize(function() {
		htmlbodyHeightUpdate()
	});
	$( window ).scroll(function() {
		height2 = $('.main').height()
			htmlbodyHeightUpdate()
	});
});

</script>
	<nav class="navbar navbar-inverse sidebar" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-sidebar-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>

				<%
			String name= (String)session.getAttribute("uname");
				int y=name.length();
				System.out.println(y+"x");
				String sub=name.substring(1, y);
				System.out.println(sub+"sub");
				char namefirstchar=name.charAt(0);
				System.out.println(namefirstchar);
				String s=String.valueOf(namefirstchar);
				String upper=s.toUpperCase();
				String newstring=upper.concat(sub);
			%>
				<a class="navbar-brand" href="#"><%=newstring %> (Admin one)</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-sidebar-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<!-- <li class="active"><a href="#">Dashboard<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li> -->
					<li><a href="#outdoor" id="submitoutdoor">
							VIEW<span style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span>
							<span class="badge" id="notification"></span>
					</a></li>
					<li><a href="resident.do?action=createadmintwo" id="createresident">Create
							Admin Two<span style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span>
					</a></li>
					<li><a href="resident.do?action=createadminthreeadminone">Create
							Admin Three<span style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span>
					</a></li>

					<li><a href="#transporter" id="submit12" value="ward"
						data-toggle="collapse">Assign TransporterToWard<span
							style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span></a></li>
					<!--  <li><a href="#viewresident" id="submit123" value="ward"
						data-toggle="collapse">View Resident<span
							style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>  -->
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown">Reports <span class="caret"></span><span
							style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
						<ul class="dropdown-menu forAnimate" role="menu">
						 <li><a href="#viewresident" id="submit123" value="ward">View Resident</a></li>
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<!--  	<li class="divider"></li>
						<li><a href="#">Separated link</a></li>
						<li class="divider"></li>
						<li><a href="#">One more separated link</a></li> -->
						</ul></li>
						<li><a href="changepassword.do?action=changepasswordone">Change
							Password<span style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span>
					</a></li>
					<li><a href="logout.do?action=logout">Logout<span
							style="font-size: 16px;"
							class="pull-right hidden-xs showopacity glyphicon glyphicon-off"></span></a></li>
				</ul>

			</div>
		</div>
	</nav>
	<div class="rightSection">
		<% 
String pageincl="";
	if(request.getAttribute("page")!=null){
	String pg=request.getAttribute("page").toString();
	
	if(pg.equals("adminonepage")){
		pageincl="admintworegsitration.jsp";
	}
	else if(pg.equals("adminthreeone")){
		pageincl="adminthreeregistration.jsp";
	}
	else if (pg.equals("changepassword")) {
		pageincl = "changepassword.jsp";
	}
}
		%>
		<%
	if(!pageincl.isEmpty()){
		
	
	%>

		<div class="col-sm-9">
			<jsp:include page='<%=pageincl %>' />
		</div>
		<%} %>
	</div>
	
	<div id="transporter" class="panel-collapse collapse">
			<h1 style="text-align: center;">Assign TransporterToWard</h1>
			<div class="col-sm-3"></div>
			<div align="center" class="col-sm-4">
				<div class="">
					<div class="dropdown">
						<label for="dropdown">Choose Ward</label> 
						<select
							class="form-control mrgn-bttm-md" id="wardDropdown"
							name="dropdown">
							</select>
						<span id="spanchooseward"></span>
					</div>

					<div class="dropdown">
						<label for="dropdown">Choose Transporter</label> <select
							class="form-control mrgn-bttm-md" id="transporterDropdown"
							name="dropdown1">
               </select>
               <span id="spanchoosetransporter"></span>
					</div>
					
					<div class="form-group">
						<label for="pwd">select date</label> <input type="date"
							class="form-control" id="dat" name="dat">
							<span id="spanchoosedate"></span>
					</div>
					<input type="hidden" value="transporterward" id="action20">
					<button type="button" class="btn btn-primary btn-block" id="transtowardAssign">Assign</button>
				</div>
				<div style="text-align: center; color: blue;" id="success20"></div>

			</div>

		</div>
	
	<div id="viewresident" class="panel-collapse collapse">
		<h1 style="text-align: center;">View Resident</h1>
		<div class="col-sm-3"></div>
		<div align="center" class="col-sm-4">
			<div class="dropdown">
				<label for="dropdown">Choose Ward</label> <select
					class="form-control mrgn-bttm-md" id="residentwardDropdown" name="dropdown">

				</select>
			</div>
			<!--   <br>
     <button type="submit" class="btn btn-primary btn-block">Submit</button>
    -->
		</div>
		<div class="col-sm-3"></div>
		<div class="col-sm-9">
			<jsp:include page="Table.jsp"></jsp:include>
		</div>
	</div>
	
	
	 <div class="col-sm-9" id="outdoor">
			<jsp:include page="outdoorimage.jsp"></jsp:include>
		</div>
</body>
</html>
