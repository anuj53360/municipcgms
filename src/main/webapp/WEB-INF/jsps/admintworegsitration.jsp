
<%--  <jsp:include page="../header/header.html" />--%>
<div class="container">

  <h2>Create Admin Two</h2>
<%
if(request.getAttribute("message")!=null){
String x=(String) request.getAttribute("message");%>
<p><% out.println(x);%></p>

<% 
}
%>

  <div class="col-sm-10">
  <form action="CreateAdminController" method="post" enctype="multipart/form-data" name="admintwo-regis-form" id="admintwo-regis-form" role="form" >
   
  <div class="form-group">
    <label for="firstName">First Name</label>
    <input type="text" class="form-control" name="resFname" id="firstName"  placeholder="Enter First Name">    
  </div>
  <div class="form-group">
    <label for="midleName">Midle Name</label>
    <input type="text" class="form-control" name="resMname" id="midleName"  placeholder="Enter Midle Name">    
  </div>
  <div class="form-group">
    <label for="lastName">Last Name</label>
    <input type="text" class="form-control" name="resLname" id="lastName"  placeholder="Enter Last Name">    
  </div>
  <div class="form-group">
    <label for="cmcMobile">Mobile</label>
    <input type="number" class="form-control" name="resMobile" id="cmcMobile"  placeholder="Enter Number">    
  </div>
   <div class="form-group">
    <label for="emailId">Email address</label>
    <input type="email" class="form-control" name="resEmail" id="emailId" placeholder="Enter Email">
     </div>
     
     
  <div class="form-group">
    <label for="photoPath">Photo</label>
    <input type="file" class="form-control" name="resPhoto" id="photoPath"  placeholder="Upload Resident Photo">    
  </div>
   
  
    <div class="form-group">
     <input type="hidden" class="form-control" name="action" value="createadmintwo"/>    
  </div>
 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>

