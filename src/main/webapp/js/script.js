$(document).ready( function(){
jQuery.validator.addMethod("laxEmail", function(value, element) {
								  // allow any non-whitespace characters as the host part
								  return this.optional( element ) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( value );
								}, 'Please enter a valid email address.');
							
						
		
$('#login-form').validate({
    rules: {
    	mobilenumber: {
            required: true
        },
        password : {
        	 required: true
        }
    },
    messages : {
    	mobilenumber: {
            required: "Mobile Number is required"
        },
        password : {
        	 required: "Password is required"
        }
    },
    	highlight:

    		function(element, errorClass) {
    		$(element).addClass(errorClass);

    		$(element.form).find(

    		"label[for=" + element.id + "]").remove(errorClass);
    		}

    		

});
							 $('#resident-regis-form').validate({
						            rules: {
						            	resNumber: {
						                    required: true
						                },
						                resFname : {
						                	 required: true
						                },
						                
						                resLname: {
						                    required: true
						                },
						                resMobile : {
						                	 required: true
						                },
						                resEmail:{
						                	required: true,
						                	laxEmail: true
						                },
						                idType: {
						                    required: true
						                },
						                idValue : {
						                	 required: true
						                }
						            },
						            messages : {
						            	resFname: {
						                    required: "First name is required"
						                },
						                resLname : {
						                	 required: "Last name is required"
						                },
						                resEmail:{
						                	required: "Email is required"
						                },
						                resNumber: {
						                    required: "CMC number is required"
						                },
						               
						                resMobile:{
						                	required: "Mobile is required"
						                },
						                idType : {
						                	 required: "ID Type is required"
						                },
						                idValue:{
						                	required: "ID value is required"
						                }
						            },
						          /*  errorPlacement: function (error, element) { 
						            	element.css('background', 'red'); 
						            	element.css("border", "1px red");
						            	error.insertAfter(element); 
						            	} */
						            	highlight:

						            		function(element, errorClass) {
						            		$(element).addClass(errorClass);

						            		$(element.form).find(

						            		"label[for=" + element.id + "]").remove(errorClass);
						            		}

						            		

						        });
							 
							 
							 $('#transport-regis-form').validate({
						            rules: {
						            	transFname: {
						                    required: true
						                },
						                transLname : {
						                	 required: true
						                },
						                
						                transMobile: {
						                    required: true
						                },
						                vehicleNO : {
						                	 required: true
						                },
						                transLicenceNo:{
						                	required: true
						                },
						                transpassword: {
						                    required: true
						                }
						            },
						            messages : {
						            	transFname: {
						                    required: "First name is required"
						                },
						                transLname : {
						                	 required: "Last name is required"
						                },
						                transMobile:{
						                	required: "Mobile is required"
						                },
						                vehicleNO: {
						                    required: "Vehicle number is required"
						                },
						               
						                transLicenceNo:{
						                	required: "License is required"
						                },
						                transpassword : {
						                	 required: "Password is required"
						                }
						                
						            },
						          /*  errorPlacement: function (error, element) { 
						            	element.css('background', 'red'); 
						            	element.css("border", "1px red");
						            	error.insertAfter(element); 
						            	} */
						            	highlight:

						            		function(element, errorClass) {
						            		$(element).addClass(errorClass);

						            		$(element.form).find(

						            		"label[for=" + element.id + "]").remove(errorClass);
						            		}

						            		

						        });
							 
							 $('#admin-regis-form').validate({
						            rules: {
						            	resFname: {
						                    required: true
						                },
						                
						                
						                resLname: {
						                    required: true
						                },
						                resMobile : {
						                	 required: true
						                },
						                resEmail:{
						                	required: true
						                },
						                resPhoto:{
						                	required:true
						                }
						                
						            },
						            messages : {
						            	resFname: {
						                    required: "First name is required"
						                },
						                resLname : {
						                	 required: "Last name is required"
						                },
						                resMobile:{
						                	required: "Mobile is required"
						                },
						                resEmail: {
						                    required: "Vehicle number is required"
						                },
						                resPhoto: {
						                    required: "photo is required"
						                },
						              },
						          /*  errorPlacement: function (error, element) { 
						            	element.css('background', 'red'); 
						            	element.css("border", "1px red");
						            	error.insertAfter(element); 
						            	} */
						            	highlight:

						            		function(element, errorClass) {
						            		$(element).addClass(errorClass);

						            		$(element.form).find(

						            		"label[for=" + element.id + "]").remove(errorClass);
						            		}

						            		

						        });
							 
							 $('#admintwo-regis-form').validate({
						            rules: {
						            	resFname: {
						                    required: true
						                },
						                resLname: {
						                    required: true
						                },
						                resMobile : {
						                	 required: true
						                },
						                resEmail:{
						                	required: true
						                },
						                
						            },
						            messages : {
						            	resFname: {
						                    required: "First name is required"
						                },
						                resLname : {
						                	 required: "Last name is required"
						                },
						                resMobile:{
						                	required: "Mobile is required"
						                },
						                resEmail: {
						                    required: "Vehicle number is required"
						                },
						              },
						          /*  errorPlacement: function (error, element) { 
						            	element.css('background', 'red'); 
						            	element.css("border", "1px red");
						            	error.insertAfter(element); 
						            	} */
						            	highlight:

						            		function(element, errorClass) {
						            		$(element).addClass(errorClass);

						            		$(element.form).find(

						            		"label[for=" + element.id + "]").remove(errorClass);
						            		}

						            		

						        });
							 

							 
							 
});