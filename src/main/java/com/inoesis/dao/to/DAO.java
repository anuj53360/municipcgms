package com.inoesis.dao.to;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.exception.ConstraintViolationException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.inoesis.beans.Admin;
import com.inoesis.beans.AdminLogin;
import com.inoesis.beans.ChangePassword;
import com.inoesis.beans.CollectedGarbage;
import com.inoesis.beans.Resident;
import com.inoesis.beans.ResidentBean;
import com.inoesis.beans.Transporter;
import com.inoesis.beans.TransporterToWard;
import com.inoesis.beans.TransporterUserDetails;
import com.inoesis.beans.User;
import com.inoesis.controller.ApplicationController;
import com.inoesis.hibernateutil.HibernateUtil;
import com.muncip.muncip.passwordencryption.EncryptPassword;

public class DAO {
	
	public  final Logger logger=Logger.getLogger(DAO.class);
	SessionFactory sessionfactory=HibernateUtil.getSessionFactory();
	
	 public  List loginadmin(AdminLogin adminlogin) {
		
		 logger.info("login admin method is called");
	Session session=sessionfactory.openSession();
	Transaction transaction=session.beginTransaction();
	EncryptPassword encryptpassword=new EncryptPassword();
String 	encryptedpassword=encryptpassword.get_SHA_256_SecurePassword(adminlogin.getPassword());
	
	
		String SQL_QUERY ="from User as u where u.userId=? AND u.password=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,adminlogin.getMobileno());
		query.setParameter(1,encryptedpassword);
		
		List list = query.list();
		transaction.commit();
		session.close();
		return list;
	}
	 
		public  String createResident(Resident resident) {
			Session session=sessionfactory.openSession();
			
			String message="";
		        try {
		        	 System.out.println("createResident DAO : "); 
		        	session.beginTransaction();
				     session.save(resident);
				     session.getTransaction().commit();
				     message="Created Successfully";
		        } catch(ConstraintViolationException e) {
		        	 System.out.println("createResident DAO : "); 
		        	message="CMC No already exist";
		        } catch(Exception e) {
		        	 System.out.println("createResident DAO : "); 
		        	message="Error occured while creating resident";
		        }
		        return message;
		}
	 
	public String createTransporter(TransporterUserDetails transporterUserDetails) {
		Session session=sessionfactory.openSession();
		String message;
		try {
			 System.out.println("createTransporter DAO : "); 
		Transaction transaction=session.beginTransaction();
		Transporter transporter=new Transporter();
		User user=new User();
		transporter.setTransporter_firstName(transporterUserDetails.getTransporter_firstName());
		transporter.setTransporter_middleName(transporterUserDetails.getTransporter_middleName());
        transporter.setTransporter_lastName(transporterUserDetails.getTransporter_lastName());
        transporter.setTransporter_imagepath(transporterUserDetails.getTransporter_imagepath());
        transporter.setTransporter_licenceno(transporterUserDetails.getTransporter_licenceno());
		transporter.setTransporter_mobile(transporterUserDetails.getTransporter_mobile());
        transporter.setTransporter_vhecileno(transporterUserDetails.getTransporter_vhecileno());
        EncryptPassword encrypt=new EncryptPassword();
        String  encryptedpassword= encrypt.get_SHA_256_SecurePassword(transporterUserDetails.getPassword());
      
        user.setActive(transporterUserDetails.getActive());
        user.setPassword(encryptedpassword);
        user.setRoleId(transporterUserDetails.getRoleId());
        user.setRoleName(transporterUserDetails.getRoleName());
        user.setUserId(transporterUserDetails.getTransporter_mobile());
        user.setName(transporterUserDetails.getTransporter_firstName().concat(transporterUserDetails.getTransporter_lastName()));
        session.save(user);
        session.save(transporter);
        transaction.commit();
        message = " Transporter created Successfully";
        session.close();
        
	} catch(ConstraintViolationException e) {
		 System.out.println("createTransporter DAO : "); 
		 message = "A transporter with this phone number already exist";
    } catch(Exception e) {
    	 System.out.println("createTransporter DAO : "); 
    	message="Error occured while creating transporter";
    }
    return message;
	}
	

	public JSONObject getWard(){
		JSONArray al = new JSONArray();
		JSONObject jobj = new JSONObject();
		Session session=sessionfactory.openSession();
		Criteria criteria=session.createCriteria(Resident.class);
		criteria.setProjection(Projections.distinct(Projections.property("ward")));
		List list=criteria.list();
		 Iterator itr=list.iterator();
       while(itr.hasNext()){
    	   JSONObject obj = new JSONObject();
			obj.put("ward", itr.next());
			al.add(obj);
        }
       jobj.put("wards", al);
		return jobj;
		
	}
	public JSONObject getWardDetails(String action){
		JSONArray al = new JSONArray();
		JSONObject jobj = new JSONObject();
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select r.cmcno,r.ward, r.firstName || ' ' || r.lastName as name from Resident r where r.ward=:value");
		query.setParameter("value", action);
		List list=query.list();
		Iterator itr=list.iterator();
		 while(itr.hasNext()){
			 JSONObject obj = new JSONObject();
			 Object a[]=(Object[])itr.next();
			 obj.put("cmcno", a[0]);
			 obj.put("ward", a[1]);
			 obj.put("name", a[2]);
				al.add(obj);
			 //System.out.println(a[0]+"/t"+a[1]+""+a[2]);
		 }
		 jobj.put("details", al);
			return jobj;
	}
	public JSONObject getAllDetails(){
		JSONArray al = new JSONArray();
		JSONObject jobj = new JSONObject();
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select r.cmcno,r.ward, r.firstName || ' ' || r.lastName as name from Resident r");
		List list=query.list();
		Iterator itr=list.iterator();
		 while(itr.hasNext()){
			 JSONObject obj = new JSONObject();
			 Object a[]=(Object[])itr.next();
			 obj.put("cmcno", a[0]);
			 obj.put("ward", a[1]);
			 obj.put("name", a[2]);
				al.add(obj);
			 //System.out.println(a[0]+"/t"+a[1]+""+a[2]);
		 }
		 jobj.put("details", al);
			return jobj;
	}
//	public static void main(String []args) {
//		DAO d=new DAO();
//		
//System.out.println(d.getWard());
//	}
	public String createAdmin(Admin admin,User user) {
		Session session=sessionfactory.openSession();
		
		Transaction transaction=session.beginTransaction();
		 
		 String message="";
	
        try {
       
        
        session.save(user);
        session.save(admin);
        message="suceffuly created";
        transaction.commit();
        session.close();
	} catch(ConstraintViolationException e) {
		 System.out.println("createTransporter DAO : "); 
		 message = "A admin with this phone number already exist";
   } catch(Exception e) {
   	 System.out.println("admin DAO is called : "); 
   	message="Error occured while creating admin";
   }
   return message;
	}
	
	
	public  String saveGarbage(HashMap<String, String> hm) {
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		Session session=sessionfactory.openSession();
		Transaction tr=session.beginTransaction();
		System.out.println("###############################################"+hm.size());
		
		for(Map.Entry<String, String> ms : hm.entrySet()){
			String cmcno=ms.getKey();
			String isCollected=ms.getValue();
			CollectedGarbage cg=new CollectedGarbage();
			cg.setCmcno(cmcno);
			cg.setDate(date);
			if(isCollected == null){
				cg.setIsCollected("n");
			}else{
				cg.setIsCollected("y"); 
			}
			session.save(cg);
		}
		
		tr.commit(); 
		session.close(); 

		return "success";
	}

	public List getCmcNo(String mobile){
		//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		//		Date date = new Date();
		long millis = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select t.wardNo from  TransporterToWard t where t.transporterId=:value and t.date=:value1");
		query.setParameter("value", mobile);
		query.setParameter("value1", date);
		List list1=query.list();
		System.out.println("******************777777"+list1);
		Iterator itr=list1.iterator();
		List list=new ArrayList();
		while(itr.hasNext()){			
			Query query1=session.createQuery("select r.cmcno from  Resident r where r.ward=:value");
			query1.setParameter("value",itr.next() );
			List list2=query1.list();
			System.out.println("****************88888888888888"+list2);
			Iterator itr1=list2.iterator();
			while(itr1.hasNext()){
				list.add(itr1.next());
			}
		}
		System.out.println("gddfghjk"+list);
		return list;
	}
	public JSONObject getTransporterDetails(){
		JSONArray al = new JSONArray();
		JSONObject jobj = new JSONObject();
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select s.transporter_mobile, s.transporter_firstName || ' ' || s.transporter_lastName as name from Transporter s");
		//query.setParameter("value", action);
		List list=query.list();
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			JSONObject obj = new JSONObject();
			Object a[]=(Object[])itr.next();
			obj.put("mobile", a[0]);
			obj.put("name", a[1]);
			al.add(obj);
			//System.out.println(a[0]+"/t"+a[1]+""+a[2]);
		}
		jobj.put("details", al);
		return jobj;
	}
	public  String saveTransporterToWard(TransporterToWard transporterToWard) {
		Session session=sessionfactory.openSession();
		session.beginTransaction();
		session.save(transporterToWard);
		session.getTransaction().commit();
		return "success";
	}
	
	public String changepassword(ChangePassword changepassword) {
		Session session=sessionfactory.openSession();
		
		User user=new User();
		EncryptPassword encryptPassword=new EncryptPassword();
		String enpassword=encryptPassword.get_SHA_256_SecurePassword(changepassword.getConformpassword());
		String message="";
		try {
		Object o=session.get(User.class,new String(changepassword.getMobile()));
		 User user1=(User)o;
		 
		 Transaction transaction=session.beginTransaction();
		 
		
		 user1.setPassword(enpassword);  
		 
		 transaction.commit();
		 
		 System.out.println("Object Updated successfully.....!!");
		 message="sucess";
		 session.close();
		}catch(Exception e) {
			e.printStackTrace();
		message="fail to save";
		}
		
		return message;
		}
	
	public String saveOutdoorCollection(com.inoesis.beans.OutdoorCollection outdoorcollection) {
		
		Session session=sessionfactory.openSession();
		String message;
		try {
			 System.out.println("saveOutdoorCollection DAO : "); 
		Transaction transaction=session.beginTransaction();
		
        
        session.save(outdoorcollection);
        transaction.commit();
        message = "outdoor collection data Saved Successfully";
        session.close();
        
	} catch(ConstraintViolationException e) {
		 System.out.println("createTransporter DAO : "); 
		 message = "outdoor collection get some problem";
    } catch(Exception e) {
    	 System.out.println("createTransporter DAO : "); 
    	message="Error occured while saving outdoor collection data";
    }
    return message;
		
		
		
		
	}
	public List getDates(String mobile){
		//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		//		Date date = new Date();
		//long millis = System.currentTimeMillis();
		//java.sql.Date date = new java.sql.Date(millis);
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select t.date from  TransporterToWard t where t.transporterId=:value");
		query.setParameter("value", mobile);
		List list1=query.list();
		System.out.println("******************777777"+list1);
		Iterator itr=list1.iterator();
		List list=new ArrayList();
		while(itr.hasNext()){			
			//Query query1=session.createQuery("select r.cmcno from  Resident r where r.ward=:value");
			//query1.setParameter("value",itr.next() );
			//List list2=query1.list();
			//System.out.println("****************88888888888888"+list2);
			//Iterator itr1=list2.iterator();
			//while(itr1.hasNext()){
				list.add(itr.next());
			}
		return list;
		}
	
	public List getOutDoorCollectionAdminThree(String ward){
		//		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		//		Date date = new Date();
		//long millis = System.currentTimeMillis();
		//java.sql.Date date = new java.sql.Date(millis);
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select o.intial_image_path,o.ward from  OutdoorCollection o where o.status=:value and o.ward=:value1");
		query.setParameter("value","N");
		query.setParameter("value1", ward);
		List list1=query.list();
		System.out.println("******************777777"+list1.toString());
		System.out.println(list1.size());
		
		return list1;
		}
	public List getOutDoorCollectionAdminTwo(){
		long millis = System.currentTimeMillis()-48*60*60*1000;
		java.sql.Date date = new java.sql.Date(millis);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String text = df.format(date);
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select o.intial_image_path,o.ward from  OutdoorCollection o where o.status=:value and o.date=:value1");
		query.setParameter("value","N");
		query.setParameter("value1", text);
		List list1=query.list();
		System.out.println("******************777777"+list1.toString());
		System.out.println(list1.size());
		
		return list1;
		}
	public List getOutDoorCollectionAdminOne(){
		long millis = System.currentTimeMillis()-72*60*60*1000;
		java.sql.Date date = new java.sql.Date(millis);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String text = df.format(date);
		Session session=sessionfactory.openSession();
		Query query=session.createQuery("select o.intial_image_path,o.ward from  OutdoorCollection o where o.status=:value and o.date=:value1");
		query.setParameter("value","N");
		query.setParameter("value1", text);
		List list1=query.list();
		System.out.println("******************777777"+list1.toString());
		System.out.println(list1.size());
		
		return list1;
		}

}
