package com.inoesis.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.inoesis.beans.TransporterToWard;
import com.inoesis.dao.to.DAO;

/**
 * Servlet implementation class ViewResidentCmcNo
 */
public class ViewResidentCmcNo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewResidentCmcNo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("sevlet cal *********************************");
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession(false);
		DAO dao=new DAO();
		
		String mobile=(String)session.getAttribute("userid");
		System.out.println("&&&&&&&&&&&&&&&&&&"+mobile);
		List list=dao.getCmcNo(mobile);
		JSONObject obj = new JSONObject();
		obj.put("cmcno", list);
		response.getWriter().write(obj.toJSONString());
		
		
		
		
	}

}
