package com.inoesis.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.inoesis.dao.to.DAO;

/**
 * Servlet implementation class ViewDetails
 */
public class ViewDetails extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String action=request.getParameter("action");
		System.out.println("servlet call"+action);
		if(action.equals("ward")) {
			
			DAO dao=new DAO();
			JSONObject jobj=dao.getWard();
			response.getWriter().write(jobj.toString());
			System.out.println(jobj);
		 
		}else if(action.equals("all")){
			DAO dao=new DAO();
			JSONObject jobj=dao.getAllDetails();
			response.getWriter().write(jobj.toString());
			System.out.println(jobj);
		}
		
		else{
			DAO dao=new DAO();
			JSONObject jobj=dao.getWardDetails(action);
			response.getWriter().write(jobj.toString());
			System.out.println(jobj);
		 
		}
	}

}
