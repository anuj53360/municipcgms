package com.inoesis.controller;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.inoesis.beans.Resident;
import com.inoesis.beans.TransporterUserDetails;
import com.inoesis.dao.to.DAO;

@MultipartConfig(maxFileSize=1000000000)
public class TransporterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	
		boolean isMultipart;
		 File file;
		 String directoryName="";
		 String url="";
		 String firstName="";
		 String imagepath="";
		 String middleName="";
		 String lastName="";
		 String mobile="";
		 String mailId="";
		 String vhecileno="";
		 String licenceno="";
		 String password="";
//		 PrintWriter out=response.getWriter();
		  // Check that we have a file upload request
		      isMultipart = ServletFileUpload.isMultipartContent(request);
		  //    response.setContentType("text/html");
		    System.out.println(isMultipart);
		   
	        //String mobilenumber="8130060513";//read from req parameter

		      DiskFileItemFactory factory = new DiskFileItemFactory();
		// Create a new file upload handler
		      ServletFileUpload up = new ServletFileUpload(factory);
		      try{ 
	    System.out.println("mmmmmmmm");
			      List fileItems = up.parseRequest(request);
				System.out.println("*****fileItems:"+fileItems.size());

			      Iterator element = fileItems.iterator();
			      System.out.println("qqqqqqqq");
			    
			      while ( element.hasNext () ) 
			      {
			    	  System.out.println("nnnnnnn");
			        FileItem fi = (FileItem)element.next();
			        
			        if (fi.isFormField()) {

			          String name = fi.getFieldName();//text1
			          if(name.equals("transFname")){
			        	  firstName = fi.getString();
			          }
			          else  if(name.equals("transMname")){
			        	  middleName= fi.getString();
			          }
			          else if(name.equals("transLname")) {
			        	  lastName=fi.getString();
			          }
			         else if(name.equals("transMobile")) {
			        	  mobile=fi.getString();
			          }
			          else if(name.equals("vehicleNO")) {
			        	 vhecileno =fi.getString();
			        	  }
			          else if(name.equals("transLicenceNo")) {
			        	  licenceno=fi.getString();
			          }
			         
			          
			        } else {
			          
			            System.out.println("bbbbbbbbb");
			             String fileName = fi.getName();
			             imagepath=request.getRealPath("/");
			    		 
			            // imagepath=imagepath+"WEB-INF\\images\\";
			             imagepath=imagepath+"WEB-INF\\";
			             
				     	     directoryName = imagepath.concat("images");
				    	    File directory = new File(directoryName);
				    	    if (! directory.exists()){
				    	        directory.mkdir();
				    	    }
			             
			             
			            String filenm="";
			            if( fileName.lastIndexOf("\\") >= 0 ){
			            	System.out.println("vvvvvvvvvvv");
			            	filenm=fileName.substring( fileName.lastIndexOf("\\"));
			             
			            }else{
			            	System.out.println("cccccccccccc");
			            	filenm=fileName;
			               
			            }
			            directoryName=directoryName+"\\"+mobile+filenm.substring(filenm.lastIndexOf("."));
			           file=new File(directoryName);
			            fi.write( file ) ;
			            System.out.println("xxxxxxxxxxx");
			            System.out.println(fileName);
			       //     out.println("Uploaded Filename: " + fileName );
			         }
			      }
		      }catch (Exception e) {
				// TODO: handle exception
		    	  e.printStackTrace();
			}
		     
		     
		     TransporterUserDetails transporterdetails=new TransporterUserDetails();
		     transporterdetails.setTransporter_firstName(firstName);
		     transporterdetails.setTransporter_middleName(middleName);
		     transporterdetails.setTransporter_lastName(lastName);
		     transporterdetails.setTransporter_imagepath(directoryName);
		     transporterdetails.setTransporter_licenceno(licenceno);
		     transporterdetails.setTransporter_mobile(mobile);
		     transporterdetails.setTransporter_vhecileno(vhecileno);
		     transporterdetails.setPassword(mobile);
		     transporterdetails.setRoleId("t100");
		     transporterdetails.setActive("Y");
		     transporterdetails.setRoleName("transporter");
		     
		     
		     DAO dao=new DAO();
		   String msg=  dao.createTransporter(transporterdetails);

		   System.out.println("Transport controller : "+msg); 
			    	request.setAttribute("transportcreatemessage", msg);
			    	//url="WEB-INF/jsps/adminthreehome.jsp";
			  
		     RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/transporter.do?action=transporterform");
			 dispatcher.forward(request, response);
		
	}

}
