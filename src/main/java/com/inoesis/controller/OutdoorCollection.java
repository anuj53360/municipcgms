package com.inoesis.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.inoesis.beans.Admin;
import com.inoesis.beans.User;
import com.inoesis.dao.to.DAO;


import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;


public class OutdoorCollection extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		String url="";
		boolean isMultipart;
		 File file;
		 String directoryName="";
		   String landmark="";
		 String imagepath="";
		 String ward="";
		 String millisecondtimedate="";
		 Date date1;
		 String newdirectoryName="";
		 
		 // this will give you today's date
		 DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		  LocalDate localDate = LocalDate.now();
		   String date=DateTimeFormatter.ofPattern("yyyMMdd").format(localDate);
		  System.out.println(date+"date");
		  //String replaceString=date.replaceAll("/","a");
		  //System.out.println(replaceString+"replaceString");
	
		
		  //getting current date and time
		   date1 = new Date();
		  System.out.println(date1+"date1");
	      //This method returns the time in millis
	      long timeMilli = date1.getTime();
	      millisecondtimedate=String.valueOf(timeMilli);
	      System.out.println(millisecondtimedate+"millisecondtimedate");
		  
		  // Check that we have a file upload request
		      isMultipart = ServletFileUpload.isMultipartContent(request);
		  //    response.setContentType("text/html");
		    System.out.println(isMultipart);
		   
	        //String mobilenumber="8130060513";//read from req parameter

		      DiskFileItemFactory factory = new DiskFileItemFactory();
		// Create a new file upload handler
		      ServletFileUpload up = new ServletFileUpload(factory);
		      try{ 
	    System.out.println("mmmmmmmm");
			      List fileItems = up.parseRequest(request);
				System.out.println("*****fileItems:"+fileItems.size());

			      Iterator element = fileItems.iterator();
			      System.out.println("qqqqqqqq");
			    
			      while ( element.hasNext () ) 
			      {
			    	  System.out.println("nnnnnnn");
			        FileItem fi = (FileItem)element.next();
			        
			        if (fi.isFormField()) {

			          String name = fi.getFieldName();//text1
			          
			           if(name.equals("outLoction")){
			        	  landmark= fi.getString();
			        	  
			          }
			           else if(name.equals("dropdown")) {
			        	   ward=fi.getString();
			        	  }
			           
			          
			           
			        } else {
			          
			            System.out.println("bbbbbbbbb");
			             String fileName = fi.getName();
			             imagepath=request.getRealPath("/");
			    		 
			             //imagepath=imagepath+"WEB-INF\\images\\";
			             imagepath=imagepath+"WEB-INF\\";
			            String foldername="outdoorcollection";
			             directoryName=imagepath.concat(foldername);
			             File directory = new File(directoryName);
			             if(!directory.exists()) {
			            	 directory.mkdir();
			             }
			             String dateFOlder="\\"+date;
			             newdirectoryName=directoryName.concat(dateFOlder);
			             File directory1 = new File(newdirectoryName);
			             if (! directory1.exists()){
				    	        directory1.mkdir();
				    	       
			             }	 
			             
			             
//			     	     directoryName = imagepath.concat(date);
//			    	    File directory = new File(directoryName);
//			    	    if (! directory.exists()){
//			    	        directory.mkdir();
//			    	        
//			    	    }
			    	        
			     	    String filenm="";
			            if( fileName.lastIndexOf("\\") >= 0 ){
			            	System.out.println("vvvvvvvvvvv");
			            	filenm=fileName.substring( fileName.lastIndexOf("\\"));
			             
			            }else{
			            	System.out.println("cccccccccccc");
			            	filenm=fileName;
			               }
			            
			            directoryName=newdirectoryName+"\\"+millisecondtimedate+filenm.substring(filenm.lastIndexOf("."));
			           file=new File(directoryName);
			            fi.write( file ) ;
			            System.out.println("xxxxxxxxxxx");
			            System.out.println(fileName);
			       //     out.println("Uploaded Filename: " + fileName );
			         }
			      }
		      }catch (Exception e) {
				// TODO: handle exception
		    	  e.printStackTrace();
			}
		     
		     System.out.println("locality"+landmark);
		     System.out.println("ward"+ward);
		    
		     com.inoesis.beans.OutdoorCollection outdoorcollection=new com.inoesis.beans.OutdoorCollection();
		     outdoorcollection.setClean_image_path("");
		     outdoorcollection.setDate(date);
		     outdoorcollection.setIntial_image_path(directoryName);
		     outdoorcollection.setLatitude("");
		     outdoorcollection.setLongitude("");
		     outdoorcollection.setStatus("N");
		     outdoorcollection.setWard(ward);
		     outdoorcollection.setLandmark(landmark);
		     
		     DAO dao=new DAO();
		   String message=  dao.saveOutdoorCollection(outdoorcollection);
		     
		     request.setAttribute("message", message);
		     
		    url="WEB-INF/jsps/outdoorCollection.jsp";
		    
		    	
		    
		    RequestDispatcher rd=request.getRequestDispatcher(url);
		    rd.forward(request, response);


	}

	}


