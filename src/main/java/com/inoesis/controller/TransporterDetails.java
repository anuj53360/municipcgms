package com.inoesis.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.inoesis.dao.to.DAO;


public class TransporterDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		String action=request.getParameter("action");
		System.out.println("servlet call"+action);
		if(action.equals("ward")) {
			DAO dao=new DAO();
		   JSONObject jobj=dao.getTransporterDetails();
			response.getWriter().write(jobj.toString());
			//System.out.println(jobj);
	}
	}
}
