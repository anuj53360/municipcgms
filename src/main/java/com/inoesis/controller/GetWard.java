package com.inoesis.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.inoesis.dao.to.DAO;

/**
 * Servlet implementation class GetWard
 */
public class GetWard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         doPost(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		System.out.println("servlet call");

		DAO dao=new DAO();
		JSONObject jobj=dao.getWard();
		response.getWriter().write(jobj.toString());
		System.out.println(jobj);
		
		
		
	}

}
