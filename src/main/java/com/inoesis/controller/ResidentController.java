package com.inoesis.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.inoesis.beans.Resident;
import com.inoesis.beans.ResidentBean;
import com.inoesis.dao.to.DAO;

@MultipartConfig(maxFileSize=1000000000)
public class ResidentController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String url="";
	boolean isMultipart;
	 File file;
	 String directoryName="";
	 String cmcno="";
	 String firstName="";
	 String imagepath="";
	 String middleName="";
	 String lastName="";
	 String mobile="";
	 String mailId="";
	 String identity_type="";
	 String identity_value="";
	 String ward="";
	 HttpSession session=request.getSession(false);
	 
//	 PrintWriter out=response.getWriter();
	  // Check that we have a file upload request
	      isMultipart = ServletFileUpload.isMultipartContent(request);
	  //    response.setContentType("text/html");
	    System.out.println(isMultipart);
	   
        //String mobilenumber="8130060513";//read from req parameter

	      DiskFileItemFactory factory = new DiskFileItemFactory();
	// Create a new file upload handler
	      ServletFileUpload up = new ServletFileUpload(factory);
	      try{ 
    System.out.println("mmmmmmmm");
		      List fileItems = up.parseRequest(request);
			System.out.println("*****fileItems:"+fileItems.size());

		      Iterator element = fileItems.iterator();
		      System.out.println("qqqqqqqq");
		    
		      while ( element.hasNext () ) 
		      {
		    	  System.out.println("nnnnnnn");
		        FileItem fi = (FileItem)element.next();
		        
		        if (fi.isFormField()) {

		          String name = fi.getFieldName();//text1
		          if(name.equals("resNumber")){
		        	  cmcno = fi.getString();
		          }
		          else  if(name.equals("resFname")){
		        	  firstName= fi.getString();
		          }
		          else if(name.equals("resMname")) {
		        	  middleName=fi.getString();
		          }
		          else if(name.equals("resLname")) {
		        	  lastName=fi.getString();
		          }
		          else if(name.equals("resMobile")) {
		        	  mobile=fi.getString();
		        	  }
		          else if(name.equals("resEmail")) {
		        	  mailId=fi.getString();
		          }
		          else if(name.equals("identityType")) {
		        	  identity_type=fi.getString();
		          }
		          else if(name.equals("idValue")) {
		        	  identity_value=fi.getString();
		          }
		          else if(name.equals("resward")) {
		        	  ward=fi.getString();
		          }
		        } else {
		          
		            System.out.println("bbbbbbbbb");
		             String fileName = fi.getName();
		             imagepath=request.getRealPath("/");
		    		 
		             //imagepath=imagepath+"WEB-INF\\images\\";
		             imagepath=imagepath+"WEB-INF\\";
		             
		     	     directoryName = imagepath.concat("images");
		    	    File directory = new File(directoryName);
		    	    if (! directory.exists()){
		    	        directory.mkdir();
		    	    }
		    	        
		     	    String filenm="";
		            if( fileName.lastIndexOf("\\") >= 0 ){
		            	System.out.println("vvvvvvvvvvv");
		            	filenm=fileName.substring( fileName.lastIndexOf("\\"));
		             
		            }else{
		            	System.out.println("cccccccccccc");
		            	filenm=fileName;
		               
		            }
		            directoryName=directoryName+"\\"+mobile+filenm.substring(filenm.lastIndexOf("."));
		           file=new File(directoryName);
		            fi.write( file ) ;
		            System.out.println("xxxxxxxxxxx");
		            System.out.println(fileName);
		       //     out.println("Uploaded Filename: " + fileName );
		         }
		      }
	      }catch (Exception e) {
			// TODO: handle exception
	    	  e.printStackTrace();
		}
	     
	     Resident resident=new Resident();
	     resident.setCmcno(cmcno);
	     resident.setFirstName(firstName);
	     resident.setMiddleName(middleName);
	     resident.setLastName(lastName);
	     resident.setIdentity_type(identity_type);
	     resident.setIdentity_value(identity_value);
	     resident.setImage_path(directoryName);
	     resident.setMobile(mobile);
	     resident.setMailId(mailId);
	     resident.setWard(ward);
	     
	     DAO dao=new DAO();
	    String msg= dao.createResident(resident);
	    System.out.println("resident controller : "+msg);
	    	request.setAttribute("residentcreatemessage", msg);
	    	//url="WEB-INF/jsps/adminthreehome.jsp";
	    	RequestDispatcher dispatcher=null;
	    	
	    	if(session.getAttribute("uname")!=null) {
	    		String z=(String)session.getAttribute("uname");
	    		System.out.println(z);
	    		System.out.println("111111");
	    		dispatcher = this.getServletContext().getRequestDispatcher("/resident.do?action=createresidentview");
	    	}
	    	else {
	    		System.out.println("22222");
	    		 dispatcher = this.getServletContext().getRequestDispatcher("/resident.do?action=frontresident");
	    	}
	    	
	    	
	    	
		    dispatcher.forward(request, response);


}



}
