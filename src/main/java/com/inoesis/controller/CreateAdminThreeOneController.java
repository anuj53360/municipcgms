package com.inoesis.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.inoesis.beans.Admin;
import com.inoesis.beans.Resident;
import com.inoesis.beans.ResidentBean;
import com.inoesis.beans.User;
import com.inoesis.dao.to.DAO;
import com.muncip.muncip.passwordencryption.EncryptPassword;

@MultipartConfig(maxFileSize=1000000000)
public class CreateAdminThreeOneController extends HttpServlet {
	private static final long serialVersionUID = 1L;
   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String url="";
	boolean isMultipart;
	 File file;
	 String directoryName="";
	   String firstName="";
	 String imagepath="";
	 String middleName="";
	 String lastName="";
	 String mobile="";
	 String mailId="";
	 String ward="";
	 
//	 PrintWriter out=response.getWriter();
	  // Check that we have a file upload request
	      isMultipart = ServletFileUpload.isMultipartContent(request);
	  //    response.setContentType("text/html");
	    System.out.println(isMultipart);
	   
        //String mobilenumber="8130060513";//read from req parameter

	      DiskFileItemFactory factory = new DiskFileItemFactory();
	// Create a new file upload handler
	      ServletFileUpload up = new ServletFileUpload(factory);
	      try{ 
    System.out.println("mmmmmmmm");
		      List fileItems = up.parseRequest(request);
			System.out.println("*****fileItems:"+fileItems.size());

		      Iterator element = fileItems.iterator();
		      System.out.println("qqqqqqqq");
		    
		      while ( element.hasNext () ) 
		      {
		    	  System.out.println("nnnnnnn");
		        FileItem fi = (FileItem)element.next();
		        
		        if (fi.isFormField()) {

		          String name = fi.getFieldName();//text1
		          
		           if(name.equals("resFname")){
		        	  firstName= fi.getString();
		          }
		          else if(name.equals("resMname")) {
		        	  middleName=fi.getString();
		          }
		          else if(name.equals("resLname")) {
		        	  lastName=fi.getString();
		          }
		          else if(name.equals("resMobile")) {
		        	  mobile=fi.getString();
		        	  }
		          else if(name.equals("resEmail")) {
		        	  mailId=fi.getString();
		          }
		          else if(name.equals("resward")) {
		        ward=fi.getString();
		          }
		           
		        } else {
		          
		            System.out.println("bbbbbbbbb");
		             String fileName = fi.getName();
		             imagepath=request.getRealPath("/");
		    		 
		             //imagepath=imagepath+"WEB-INF\\images\\";
		             imagepath=imagepath+"WEB-INF\\";
		             
		     	     directoryName = imagepath.concat("images");
		    	    File directory = new File(directoryName);
		    	    if (! directory.exists()){
		    	        directory.mkdir();
		    	    }
		    	        
		     	    String filenm="";
		            if( fileName.lastIndexOf("\\") >= 0 ){
		            	System.out.println("vvvvvvvvvvv");
		            	filenm=fileName.substring( fileName.lastIndexOf("\\"));
		             
		            }else{
		            	System.out.println("cccccccccccc");
		            	filenm=fileName;
		               
		            }
		            directoryName=directoryName+"\\"+mobile+filenm.substring(filenm.lastIndexOf("."));
		           file=new File(directoryName);
		            fi.write( file ) ;
		            System.out.println("xxxxxxxxxxx");
		            System.out.println(fileName);
		       //     out.println("Uploaded Filename: " + fileName );
		         }
		      }
	      }catch (Exception e) {
			// TODO: handle exception
	    	  e.printStackTrace();
		}
	     
	     Admin admin=new Admin();
	     admin.setAdmin_firstName(firstName);
	     admin.setAdmin_lastName(lastName);
	     admin.setAdmin_middleName(middleName);
	     admin.setAdmin_mobileno(mobile);
	     admin.setAdmin_email(mailId);
	     admin.setAdmin_designation("admin3");
	    
	     User user=new User();
	     user.setRoleName(admin.getAdmin_designation());
	     user.setRoleId("300");
	     user.setWard(ward);
	     EncryptPassword encrypt=new EncryptPassword();
	        String  encryptedpassword= encrypt.get_SHA_256_SecurePassword(admin.getAdmin_mobileno());
	     
	     
	     user.setRoleName(admin.getAdmin_designation());
	     
	     user.setActive("Y");
	        user.setPassword(encryptedpassword);
	        user.setWard(admin.getWard());
	        System.out.println(user.getRoleName()+"user.getRoleName()");
	        
	        user.setUserId(admin.getAdmin_mobileno());
	        user.setName(admin.getAdmin_firstName().concat(admin.getAdmin_lastName()));
	     
	     DAO dao=new DAO();
	    String msg= dao.createAdmin(admin,user);
	    
	    	request.setAttribute("message", msg);
	    	request.setAttribute("page", "admintwopage");
	    	url="WEB-INF/jsps/adminonehome.jsp";
	   
	    RequestDispatcher rd=request.getRequestDispatcher(url);
	    rd.forward(request, response);


}



}
