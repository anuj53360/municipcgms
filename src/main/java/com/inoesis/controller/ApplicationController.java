package com.inoesis.controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.hibernate.Session;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


import com.inoesis.beans.AdminLogin;
import com.inoesis.beans.ChangePassword;
import com.inoesis.beans.Resident;
import com.inoesis.beans.TransporterToWard;
import com.inoesis.beans.User;
import com.inoesis.dao.to.DAO;
import com.inoesis.validation.FormValidation;

@MultipartConfig(maxFileSize=100000000)
public class ApplicationController extends HttpServlet {
	private static final long serialVersionUID = 102831973239L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("do get method is called");
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		final Logger logger=Logger.getLogger(ApplicationController.class);
		//System.out.println("do post method is called");
		logger.info("application servlet is called post method");


		String url=null;
		String action="";
		System.out.println(request.getParameter("action")+" "+"action");
		if(request.getParameter("action")!=null){
			action=request.getParameter("action");

			if(action.equals("loginview")){
				logger.info("login view if blocked is called");

				url="WEB-INF/jsps/login.jsp";
			}else if(action.equals("resident")){
				logger.info("resident registration is called");

				url="WEB-INF/jsps/residentregistration.jsp";
			}


			else if(action.equals("login")) {
				HttpSession session=request.getSession(false);
				if(session!=null) {
					session.invalidate();
				}
				session=request.getSession(true);
				logger.info("login if block is called ");
				String mobilenumber=request.getParameter("mobilenumber");
				String password=request.getParameter("password");


				AdminLogin adminlogin=new AdminLogin();
				adminlogin.setMobileno(mobilenumber);
				adminlogin.setPassword(password);


				FormValidation formvalidation=new FormValidation();
				formvalidation.loginForm(adminlogin);

				DAO dao=new DAO(); 
				List list=dao.loginadmin(adminlogin);
				System.out.println("*************USER list"+list);


				if(list!=null && list.size()>0) {
					User u=(User)list.get(0);
					String name=u.getName();
					String role=u.getRoleName();
					String mobile=u.getUserId();
					String ward=u.getWard();
					System.out.println(ward);
					session.setAttribute("uname", name);
					session.setAttribute("role", role);
					session.setAttribute("userid", mobile);
					session.setAttribute("ward", ward);

					if(role.equals("admin3")) {
						url="WEB-INF/jsps/adminthreehome.jsp";
					}
					else if(role.equals("transporter")) {
						url="WEB-INF/jsps/transporterhome.jsp";
					}
					else if(role.equals("admin2")) {
						url="WEB-INF/jsps/admintwohome.jsp";
					}
					else if(role.equals("admin1")) {
						url="WEB-INF/jsps/adminonehome.jsp";
					}


					logger.info("login is sucessfull");

				}

				else {
					String message="Please Check your UserId or Password ";
					request.setAttribute("errormessage", message);
					url="WEB-INF/jsps/login.jsp";
					
					logger.info("data is not getting from database");
				}
			}

			else if(action.equals("createresidentview")){

				request.setAttribute("page", "resident");
				url="WEB-INF/jsps/adminthreehome.jsp";
			}
			else if(action.equals("transporterform")){

				request.setAttribute("page", "transporter");
				url="WEB-INF/jsps/adminthreehome.jsp";
			}
			
			else if(action.equals("logout")){

				HttpSession session=request.getSession(false);
				session.invalidate();
			//response.sendRedirect("index.jsp");
				url="index.jsp";
			}
			else if(action.equals("createadminthree")){

				request.setAttribute("page", "admintwopage");
				url="WEB-INF/jsps/admintwohome.jsp";
			}
			else if(action.equals("createadmintwo")){

				request.setAttribute("page", "adminonepage");
				url="WEB-INF/jsps/adminonehome.jsp";
			}		
			else if(action.equals("createadminthreeadminone")){

				request.setAttribute("page", "adminthreeone");
				url="WEB-INF/jsps/adminonehome.jsp";
			}	
			else if(action.equals("collectgc")){
				System.out.println("c1al %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
				HashMap<String, String>hm=new HashMap<String, String>();
				Enumeration<String> names=request.getParameterNames();
				while(names.hasMoreElements()){
					String nm=names.nextElement();

					if(nm.equals("action")||nm.equals("on")){

					}
					else{
						String cmc=request.getParameter(nm);
						System.out.println("cmc:"+cmc);
						if(!cmc.equals("on")){
							String iscollected=request.getParameter(cmc+"c");
							hm.put(cmc, iscollected);
							System.out.println("before dao@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+hm.size());			
						}
					}
					System.out.println("************************************************************************** just before dao"+hm.size());



				}
				DAO dao=new DAO();
				dao.saveGarbage(hm);
				request.setAttribute("status", "successfully saved");
				url="WEB-INF/jsps/transporterhome.jsp";
			}

			else if(action.equals("changepassword")) {
				logger.info("change password else if block is called");
				request.setAttribute("page", "changepassword"); 
				url="WEB-INF/jsps/adminthreehome.jsp";	
			}
			else if(action.equals("changepasswordtwo")) {
				logger.info("change password else if block is called");
				request.setAttribute("page", "changepassword"); 
				url="WEB-INF/jsps/admintwohome.jsp";	
			}
			else if(action.equals("changepasswordone")) {
				logger.info("change password else if block is called");
				request.setAttribute("page", "changepassword"); 
				url="WEB-INF/jsps/adminonehome.jsp";	
			}
			else if(action.equals("changepasswordtransporter")) {
				logger.info("change password else if block is called");
				request.setAttribute("page", "changepassword"); 
				url="WEB-INF/jsps/transporterhome.jsp";	
			}
			else if(action.equals("frontresident")){
				logger.info("resident registration is called");

				url="WEB-INF/jsps/frontresidentregistration.jsp";
			}
			
			else if(action.equals("chnagepasswordform")){
				logger.info("change password form is called");
				String password=request.getParameter("newpassword");
				String confpassword=request.getParameter("conformpassword");
	            HttpSession session=request.getSession(false);
	           String mobile=(String) session.getAttribute("userid");
	           String role=(String) session.getAttribute("role");
				if(password.equals(confpassword)) {
					ChangePassword changepassword=new ChangePassword();
					changepassword.setNewpassword(password);
					changepassword.setConformpassword(confpassword);
					changepassword.setMobile(mobile);
					DAO dao=new DAO();
				String message=	dao.changepassword(changepassword);
				request.setAttribute("message",message );
				request.setAttribute("page", "changepassword");
				
				if(role.equals("admin3")) {
					url="WEB-INF/jsps/adminthreehome.jsp";
				}
				else if(role.equals("transporter")) {
					url="WEB-INF/jsps/transporterhome.jsp";
				}
				else if(role.equals("admin2")) {
					url="WEB-INF/jsps/admintwohome.jsp";
				}
				else if(role.equals("admin1")) {
					url="WEB-INF/jsps/adminonehome.jsp";
				}
				
				}

				
			}else if(action.equals("transporterward")){
				String ward=request.getParameter("dropdown");
				String mobile=request.getParameter("dropdown1");
				String date=request.getParameter("dat");
				System.out.println("ddddddddddddddaaaaaaaattttttteeeeeeee"+date);
				TransporterToWard tw=new TransporterToWard();
				tw.setWardNo(ward);
				tw.setTransporterId(mobile);
				tw.setDate(Date.valueOf(date));
				
				DAO dao=new DAO();
				String msg=dao.saveTransporterToWard(tw);
			  System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+msg);
			  request.setAttribute("status", "successfully saved");
				url="WEB-INF/jsps/adminthreehome.jsp";
			}
			else if(action.equals("outdoorgarbage")) {
				url="WEB-INF/jsps/outdoorCollection.jsp";
			}



		}
		if(url!=null){
			RequestDispatcher rd=request.getRequestDispatcher(url);
			rd.forward(request, response);
		}

		if(request.getParameter("dropdown")!=null){
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			String ward=request.getParameter("dropdown");
			String mobile=request.getParameter("dropdown1");
			System.out.println("mmmmmmmmmmmmmmmbbbbbbbbbbbbbbbbbbbiiiiiiiiieeeeeeeeee"+mobile);
			String date=request.getParameter("dat");
			DAO daoDate=new DAO();
			List list=daoDate.getDates(mobile);
			JSONObject obj=new JSONObject();
			Iterator itr=list.iterator();
			int count = 0;
			if(list.isEmpty()){
				TransporterToWard tw=new TransporterToWard();
				tw.setWardNo(ward);
				tw.setTransporterId(mobile);
				tw.setDate(Date.valueOf(date));
				DAO dao=new DAO();
				String msg=dao.saveTransporterToWard(tw);
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+msg);
				obj.put("status", "Successfully Assigned");
				count++;
			}
			while(itr.hasNext()){
				Date date1=(Date)itr.next();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String text = df.format(date1);
				System.out.println("dddddddddddddddddaaaaaaaaaaattttttttttteeeeeeeeeee"+date);
				System.out.println("dddbb dddddddddddddaaaaaaaaaatttteeeeeeeeee"+text);
				if(date.equals(text)){
					System.out.println("aleady exists");
					obj.put("status", "Aleady assigned in today's date");
					count++;
				}
			}
			if(count==0){
				System.out.println("call else *****************************************");
				TransporterToWard tw=new TransporterToWard();
				tw.setWardNo(ward);
				tw.setTransporterId(mobile);
				tw.setDate(Date.valueOf(date));
				DAO dao=new DAO();  
				String msg=dao.saveTransporterToWard(tw);
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+msg);
				obj.put("status", "Successfully Assigned");
			}
			response.getWriter().write(obj.toJSONString());
		}
		
		if(request.getParameter("actionout")!=null){
			System.out.println("notificationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			HttpSession session=request.getSession(false);
			String ward=(String)session.getAttribute("ward");
			System.out.println(ward+"ward");
			List list=new DAO().getOutDoorCollectionAdminThree(ward);
			System.out.println("ssssssssssssssiiiiiiiiiiiiiizzzzzzzzzzzeeeeeeeeeeeeeeeeee"+list.size());
			JSONObject obj=new JSONObject();
			obj.put("notify", list.size());

			response.getWriter().write(obj.toJSONString());
		}if(request.getParameter("actionresult")!=null){
			System.out.println("notificationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			HttpSession session=request.getSession(false);
			String ward=(String)session.getAttribute("ward");
			System.out.println(ward+"ward");
			List list=new DAO().getOutDoorCollectionAdminThree(ward);
			Iterator itr=list.iterator();
			JSONObject obj1=new JSONObject();
			JSONArray jsa=new JSONArray();
			while(itr.hasNext()){
				JSONObject obj=new JSONObject();
				Object[] ob=(Object[])itr.next();
				obj.put("imagepath", ob[0]);
				obj.put("ward", ob[1]);
				jsa.add(obj);
			}
			obj1.put("outdoordetails", jsa);
			response.getWriter().write(obj1.toJSONString());
		}
		
		if(request.getParameter("actiontwoout")!=null){
			System.out.println("notificationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			List list=new DAO().getOutDoorCollectionAdminTwo();
			System.out.println("ssssssssssssssiiiiiiiiiiiiiizzzzzzzzzzzeeeeeeeeeeeeeeeeee"+list.size());
			JSONObject obj=new JSONObject();
			obj.put("notify", list.size());

			response.getWriter().write(obj.toJSONString());
		}if(request.getParameter("actiontworesult")!=null){
			System.out.println("notificationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			List list=new DAO().getOutDoorCollectionAdminTwo();
			Iterator itr=list.iterator();
			JSONObject obj1=new JSONObject();
			JSONArray jsa=new JSONArray();
			while(itr.hasNext()){
				JSONObject obj=new JSONObject();
				Object[] ob=(Object[])itr.next();
				obj.put("imagepath", ob[0]);
				obj.put("ward", ob[1]);
				jsa.add(obj);
			}
			obj1.put("outdoordetails", jsa);
			response.getWriter().write(obj1.toJSONString());
		}
		if(request.getParameter("actiononeout")!=null){
			System.out.println("notificationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			List list=new DAO().getOutDoorCollectionAdminOne();
			System.out.println("ssssssssssssssiiiiiiiiiiiiiizzzzzzzzzzzeeeeeeeeeeeeeeeeee"+list.size());
			JSONObject obj=new JSONObject();
			obj.put("notify", list.size());

			response.getWriter().write(obj.toJSONString());
		}if(request.getParameter("actiononeresult")!=null){
			System.out.println("notificationnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			List list=new DAO().getOutDoorCollectionAdminOne();
			Iterator itr=list.iterator();
			JSONObject obj1=new JSONObject();
			JSONArray jsa=new JSONArray();
			while(itr.hasNext()){
				JSONObject obj=new JSONObject();
				Object[] ob=(Object[])itr.next();
				obj.put("imagepath", ob[0]);
				obj.put("ward", ob[1]);
				jsa.add(obj);
			}
			obj1.put("outdoordetails", jsa);
			response.getWriter().write(obj1.toJSONString());
		}

	}
}
