package com.inoesis.beans;

public class TransporterUserDetails {
	
	private String transporter_mobile;
	private String transporter_firstName;
	private String transporter_middleName;
	private String transporter_lastName;
	private String transporter_vhecileno;
	private String transporter_imagepath;
	private String transporter_licenceno;
	private String userId;
	private String password;
	private String roleId;
	private String active;
	private String roleName;
	public String getTransporter_mobile() {
		return transporter_mobile;
	}
	public void setTransporter_mobile(String transporter_mobile) {
		this.transporter_mobile = transporter_mobile;
	}
	public String getTransporter_firstName() {
		return transporter_firstName;
	}
	public void setTransporter_firstName(String transporter_firstName) {
		this.transporter_firstName = transporter_firstName;
	}
	public String getTransporter_middleName() {
		return transporter_middleName;
	}
	public void setTransporter_middleName(String transporter_middleName) {
		this.transporter_middleName = transporter_middleName;
	}
	public String getTransporter_lastName() {
		return transporter_lastName;
	}
	public void setTransporter_lastName(String transporter_lastName) {
		this.transporter_lastName = transporter_lastName;
	}
	public String getTransporter_vhecileno() {
		return transporter_vhecileno;
	}
	public void setTransporter_vhecileno(String transporter_vhecileno) {
		this.transporter_vhecileno = transporter_vhecileno;
	}
	public String getTransporter_imagepath() {
		return transporter_imagepath;
	}
	public void setTransporter_imagepath(String transporter_imagepath) {
		this.transporter_imagepath = transporter_imagepath;
	}
	public String getTransporter_licenceno() {
		return transporter_licenceno;
	}
	public void setTransporter_licenceno(String transporter_licenceno) {
		this.transporter_licenceno = transporter_licenceno;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	}
