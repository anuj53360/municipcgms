package com.inoesis.beans;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.inoesis.hibernateutil.HibernateUtil;
import com.muncip.muncip.passwordencryption.EncryptPassword;

public class UserMain {

	public static void main(String[] args) {
		
		SessionFactory sf=HibernateUtil.getSessionFactory();
		Session s=sf.openSession();
		Transaction t=s.beginTransaction();
		
		User u=new User();
		EncryptPassword ep=new EncryptPassword();
	String x=	ep.get_SHA_256_SecurePassword("anuj");
		//u.setRoleId("8130060515");
		u.setPassword(x);
		u.setRoleId("601");
        u.setUserId("8130060515");
        u.setRoleName("admin3");
        u.setActive("Y");
        
        s.save(u);
        t.commit();
        s.close();
 sf.close();       
        
	}

}
