package com.inoesis.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="transporter")


public class Transporter implements Serializable {
	
	@Id
	@Column(name="transporter_mobile")
	private String transporter_mobile;
	
	@Column(name="transporter_firstName")
	private String transporter_firstName;
	
	@Column(name="transporter_middleName")
	private String transporter_middleName;
	
	@Column(name="transporter_lastName")
	private String transporter_lastName;
	
	@Column(name="transporter_vhecileno")
	private String transporter_vhecileno;
	
	@Column(name="transporter_imagepath")
	private String transporter_imagepath;
	
	@Column(name="transporter_licenceno")
	private String transporter_licenceno;
	
	
	
	public String getTransporter_mobile() {
		return transporter_mobile;
	}
	public void setTransporter_mobile(String transporter_mobile) {
		this.transporter_mobile = transporter_mobile;
	}
	
	
	public String getTransporter_vhecileno() {
		return transporter_vhecileno;
	}
	public void setTransporter_vhecileno(String transporter_vhecileno) {
		this.transporter_vhecileno = transporter_vhecileno;
	}
	
	public String getTransporter_firstName() {
		return transporter_firstName;
	}
	public void setTransporter_firstName(String transporter_firstName) {
		this.transporter_firstName = transporter_firstName;
	}
	public String getTransporter_middleName() {
		return transporter_middleName;
	}
	public void setTransporter_middleName(String transporter_middleName) {
		this.transporter_middleName = transporter_middleName;
	}
	public String getTransporter_lastName() {
		return transporter_lastName;
	}
	public void setTransporter_lastName(String transporter_lastName) {
		this.transporter_lastName = transporter_lastName;
	}
	public String getTransporter_imagepath() {
		return transporter_imagepath;
	}
	public void setTransporter_imagepath(String transporter_imagepath) {
		this.transporter_imagepath = transporter_imagepath;
	}
	public String getTransporter_licenceno() {
		return transporter_licenceno;
	}
	public void setTransporter_licenceno(String transporter_licenceno) {
		this.transporter_licenceno = transporter_licenceno;
	}
	
	public Transporter() {
		
	}
	
	@Override
	public String toString() {
		return "Transporter [transporter_mobile=" + transporter_mobile + ", transporter_id="
				+ ", transporter_firstName=" + transporter_firstName + ", transporter_middleName="
				+ transporter_middleName + ", transporter_lastName=" + transporter_lastName + ", transporter_vhecileno="
				+ transporter_vhecileno + ", transporter_imagepath=" + transporter_imagepath
				+ ", transporter_licenceno=" + transporter_licenceno + "]";
	}
	}
