package com.inoesis.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="outdoorcollection")

public class OutdoorCollection implements Serializable {
    
	@Id
	@Column(name="intialimagepath")
	private String intial_image_path;
	
	@Column(name="cleanimagepath")
	private String clean_image_path;
	
	@Column(name="ward")
	private String ward;
	
	@Column(name="latitude")
	private String latitude;
	
	@Column(name="longitude")
	private String longitude;
	
	@Column(name="date")
	private String date;
	
	@Column(name="status")
	private String status;
	
	@Column(name="landmark")
	private String landmark;

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getIntial_image_path() {
		return intial_image_path;
	}

	public void setIntial_image_path(String intial_image_path) {
		this.intial_image_path = intial_image_path;
	}

	public String getClean_image_path() {
		return clean_image_path;
	}

	public void setClean_image_path(String clean_image_path) {
		this.clean_image_path = clean_image_path;
	}

	public String getWard() {
		return ward;
	}

	public void setWard(String ward) {
		this.ward = ward;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public OutdoorCollection() {
		
	}

	@Override
	public String toString() {
		return "OutdoorCollection [intial_image_path=" + intial_image_path + ", clean_image_path=" + clean_image_path
				+ ", ward=" + ward + ", latitude=" + latitude + ", longitude=" + longitude + ", date=" + date
				+ ", status=" + status + ", landmark=" + landmark + "]";
	}}
