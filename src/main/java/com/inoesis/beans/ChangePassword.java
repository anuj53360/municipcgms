package com.inoesis.beans;

public class ChangePassword {
	
    private String newpassword;
    private String conformpassword;
    private String mobile;
    
    
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getNewpassword() {
		return newpassword;
	}
	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}
	public String getConformpassword() {
		return conformpassword;
	}
	public void setConformpassword(String conformpassword) {
		this.conformpassword = conformpassword;
	}
	
	
	
	@Override
	public String toString() {
		return "ChangePassword [newpassword=" + newpassword + ", conformpassword=" + conformpassword + ", mobile="
				+ mobile + "]";
	}
	public ChangePassword() {
		
	}
    
    

}
