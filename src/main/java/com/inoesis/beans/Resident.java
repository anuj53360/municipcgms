package com.inoesis.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "resident")

public class Resident {

	@Id
    private String cmcno;
    private String firstName;
    private String middleName;
    private String lastName;
    private String mobile;
    private String mailId;
    private String identity_type;
    private String identity_value;
    private String image_path;
    private String ward;
	public String getCmcno() {
		return cmcno;
	}
	public void setCmcno(String cmcno) {
		this.cmcno = cmcno;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public String getIdentity_type() {
		return identity_type;
	}
	public void setIdentity_type(String identity_type) {
		this.identity_type = identity_type;
	}
	public String getIdentity_value() {
		return identity_value;
	}
	public void setIdentity_value(String identity_value) {
		this.identity_value = identity_value;
	}
	public String getImage_path() {
		return image_path;
	}
	public void setImage_path(String image_path) {
		this.image_path = image_path;
	}
	public String getWard() {
		return ward;
	}
	public void setWard(String ward) {
		this.ward = ward;
	}
	
	
    
}
