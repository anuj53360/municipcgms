package com.inoesis.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="admin")
public class Admin implements Serializable {

	@Column(name="admin_first_name")
	private String admin_firstName;
	
    @Column(name="admin_middle_name") 
	private String admin_middleName;
	
	public String getAdmin_designation() {
		return admin_designation;
	}
	public void setAdmin_designation(String admin_designation) {
		this.admin_designation = admin_designation;
	}

	@Column(name="admin_last_name")
	private String admin_lastName;
	
	@Column
	private String admin_email;
	
	@Id
	@Column
	private String admin_mobileno;
	
	@Column(name="admin_designation")
	private String admin_designation;
	
	@Column(name="ward")
	private String ward;
	
	
	public String getWard() {
		return ward;
	}
	public void setWard(String ward) {
		this.ward = ward;
	}
	public String getAdmin_firstName() {
		return admin_firstName;
	}
	public void setAdmin_firstName(String admin_firstName) {
		this.admin_firstName = admin_firstName;
	}
	public String getAdmin_middleName() {
		return admin_middleName;
	}
	public void setAdmin_middleName(String admin_middleName) {
		this.admin_middleName = admin_middleName;
	}
	public String getAdmin_lastName() {
		return admin_lastName;
	}
	public void setAdmin_lastName(String admin_lastName) {
		this.admin_lastName = admin_lastName;
	}
	public String getAdmin_email() {
		return admin_email;
	}
	public void setAdmin_email(String admin_email) {
		this.admin_email = admin_email;
	}
	public String getAdmin_mobileno() {
		return admin_mobileno;
	}
	public void setAdmin_mobileno(String admin_mobileno) {
		this.admin_mobileno = admin_mobileno;
	}
	
	public Admin() {
		
	}
	@Override
	public String toString() {
		return "Admin [admin_firstName=" + admin_firstName + ", admin_middleName=" + admin_middleName
				+ ", admin_lastName=" + admin_lastName + ", admin_email=" + admin_email + ", admin_mobileno="
				+ admin_mobileno + ", admin_designation=" + admin_designation + ", ward=" + ward + "]";
	}
	

	

	
	
	


	
	
	
}
