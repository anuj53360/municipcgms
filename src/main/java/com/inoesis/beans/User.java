package com.inoesis.beans;



import javax.persistence.*;
import java.io.Serializable;



@Entity
@Table(name = "users")

public class User implements Serializable {

	
@Id
@Column
private String userId;

@Column
private String password;

@Column
private String roleId;

@Column
private String roleName;

@Column
private String active;

@Column
private String name;

@Column
private String ward;



public String getWard() {
	return ward;
}

public void setWard(String ward) {
	this.ward = ward;
}

public String getUserId() {
	return userId;
}

public void setUserId(String userId) {
	this.userId = userId;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getRoleId() {
	return roleId;
}

public void setRoleId(String roleId) {
	this.roleId = roleId;
}

public String getRoleName() {
	return roleName;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public void setRoleName(String roleName) {
	this.roleName = roleName;
}

public String getActive() {
	return active;
}

public void setActive(String active) {
	this.active = active;
}





@Override
public String toString() {
	return "User [userId=" + userId + ", password=" + password + ", roleId=" + roleId + ", roleName=" + roleName
			+ ", active=" + active + ", name=" + name + ", ward=" + ward + "]";
}

public User() {
	
}






}

