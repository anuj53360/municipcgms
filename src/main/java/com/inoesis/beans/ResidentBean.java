package com.inoesis.beans;

public class ResidentBean {
	
	private String cmcno;
    private String firstName;
    private String middleName;
    private String lastName;
    private String mobile;
    private String mailId;
    private String identity_type;
    private String identity_value;
    private String imagepath;
    private String ward;
	public String getCmcno() {
		return cmcno;
	}
	public void setCmcno(String cmcno) {
		this.cmcno = cmcno;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public String getIdentity_type() {
		return identity_type;
	}
	public void setIdentity_type(String identity_type) {
		this.identity_type = identity_type;
	}
	public String getIdentity_value() {
		return identity_value;
	}
	public void setIdentity_value(String identity_value) {
		this.identity_value = identity_value;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public String getWard() {
		return ward;
	}
	public void setWard(String ward) {
		this.ward = ward;
	}
	public ResidentBean() {
		
	}
	

}
