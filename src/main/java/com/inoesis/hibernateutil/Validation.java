package com.inoesis.hibernateutil;

import java.util.Map;

import com.inoesis.beans.Resident;

public class Validation {

	public static boolean validateRegisterForm(Resident resident,Map errorMap){
		boolean errorExist = false;
		if(resident.getCmcno() == null || resident.getCmcno()==""){
			errorMap.put("cmcno", "Enter cmc number");
			errorExist = true;
		}
		if(resident.getFirstName() == null || resident.getFirstName()==""){
			errorMap.put("firstName", "Enter first Name");
			errorExist = true;
		}
		
		if(resident.getLastName() == null || resident.getLastName()==""){
			errorMap.put("LastName", "Enter Last Name");
			errorExist = true;
		}
		
		if(resident.getMailId() == null || resident.getMailId()==""){
			errorMap.put("email", "Enter email");
			errorExist = true;
		}else {
			  String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		      Boolean b = resident.getMailId().matches(EMAIL_REGEX);
		      if(!resident.getMailId().matches(EMAIL_REGEX)){
		    	  errorMap.put("invalidEmailFormat", "Invalid email format");
		    	  errorExist = true;
		      }
		     
		}
		
		if(resident.getIdentity_type() == null || resident.getIdentity_type()==""){
			errorMap.put("identity", "choose identity");
			errorExist = true;
		}
		if(resident.getIdentity_value() == null || resident.getIdentity_value()==""){
			errorMap.put("identity", "enter identity value");
			errorExist = true;
		}
		if(resident.getImage_path() == null || resident.getImage_path()==""){
			errorMap.put("image", "choose image");
			errorExist = true;
		}
		
		
		if(resident.getMobile() == null || resident.getMobile()==""){
			errorMap.put("phone", "Enter Phone");
			errorExist = true;
		}
		if(resident.getWard() == null || resident.getWard()==""){
			errorMap.put("ward", "Enter ward");
			errorExist = true;
		}

		return errorExist;
}
	}
